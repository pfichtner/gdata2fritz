package com.github.pfichtner.gdata2fritz.fritzdata.impl;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gc2fritz.data.api.Person;
import com.github.pfichtner.gc2fritz.data.api.PhoneNumber;
import com.github.pfichtner.gc2fritz.data.api.PhoneNumberType;
import com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Number;
import com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Telephony;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class Converter {

	private static final String HOME = "home";

	private static final BiMap<String, PhoneNumberType> map = ImmutableBiMap
			.of(HOME, PhoneNumberType.HOME, "mobile", PhoneNumberType.MOBILE,
					"work", PhoneNumberType.WORK);
	private static final Function<String, PhoneNumberType> mappingA = Functions
			.forMap(map, null);
	// map unknown to "home" (german "Privat"). This is also default in the
	// fritzbox UI
	private static final Function<PhoneNumberType, String> mappingB = Functions
			.forMap(map.inverse(), HOME);

	public static final Function<Contact, com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact> toMapping = new Function<Contact, com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact>() {
		@Override
		public com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact apply(
				Contact contact) {
			return toFritzBox(contact);
		};
	};

	private static com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact toFritzBox(
			Contact contact) {
		com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact result = new com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact();
		result.setCategory(contact.getCategory());
		com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Person person = new com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Person();
		person.setRealName(contact.getPerson().getRealName());
		result.setPerson(person);
		Telephony telephony = new Telephony();
		result.setTelephony(telephony);
		for (PhoneNumber phoneNumber : contact.getPhoneNumbers()) {
			com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Number jaxbNum = new com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Number();
			jaxbNum.setType(mappingB.apply(phoneNumber.getType()));
			jaxbNum.setVanity(phoneNumber.getVanity());
			jaxbNum.setQuickdial(phoneNumber.getQuickdial());
			jaxbNum.setPrio(phoneNumber.getPrio());
			jaxbNum.setContent(phoneNumber.getValue());
			telephony.getNumber().add(jaxbNum);
		}
		return result;
	}

	public static final Function<com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact, Contact> fromMapping = new Function<com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact, Contact>() {
		@Override
		public Contact apply(
				com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact contact) {
			return fromFritzBox(contact);
		}

	};

	private static Contact fromFritzBox(
			com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact contact) {
		Contact result = new Contact();
		result.setCategory(contact.getCategory());
		Person person = new Person();
		person.setRealName(contact.getPerson().getRealName());
		result.setPerson(person);
		Telephony telephony = contact.getTelephony();
		if (telephony != null) {
			for (Number phoneNumber : telephony.getNumber()) {
				result.addPhoneNumber(fromFritzBox(phoneNumber));
			}
		}
		return result;
	}

	private static PhoneNumber fromFritzBox(Number jaxbNum) {
		PhoneNumber phoneNumber = new PhoneNumber();
		phoneNumber.setType(mappingA.apply(jaxbNum.getType()));
		phoneNumber.setVanity(jaxbNum.getVanity());
		phoneNumber.setQuickdial(jaxbNum.getQuickdial());
		phoneNumber.setPrio(jaxbNum.getPrio());
		phoneNumber.setValue(jaxbNum.getContent());
		return phoneNumber;
	};

}
