package com.github.pfichtner.gdata2fritz.fritzdata.api;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.List;

import javax.annotation.Nonnull;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Phonebook;
import com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Phonebooks;
import com.github.pfichtner.gdata2fritz.fritzdata.impl.Converter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class FritzboxXml {

	public static <T extends OutputStream> T writePhonebook(
			Iterable<Contact> contacts, T outputStream) throws JAXBException,
			PropertyException, IOException {
		writeXml(createPhonebook(contacts), outputStream);
		return outputStream;

	}

	public static <T extends Writer> T writePhonebook(
			Iterable<Contact> contacts, T writer) throws JAXBException,
			PropertyException, IOException {
		writeXml(createPhonebook(contacts), writer);
		return writer;

	}

	// ------------------------------------------------------------------------------
	public static <T extends OutputStream> T writePhonebooks(
			Iterable<Contact> contacts, T outputStream) throws JAXBException,
			PropertyException, IOException {
		writeXml(
				createPhonebooks(Lists.newArrayList(createPhonebook(contacts))),
				outputStream);
		return outputStream;

	}

	public static <T extends Writer> T writePhonebooks(
			Iterable<Contact> contacts, T writer) throws JAXBException,
			PropertyException, IOException {
		writeXml(
				createPhonebooks(Lists.newArrayList(createPhonebook(contacts))),
				writer);
		return writer;

	}

	// ------------------------------------------------------------------------------

	private static <T extends OutputStream> T writeXml(Phonebook phonebook,
			T outputStream) throws JAXBException, PropertyException,
			IOException {
		createPhonebookMarshaller().marshal(phonebook, outputStream);
		return outputStream;

	}

	private static <T extends Writer> T writeXml(Phonebook phonebook, T writer)
			throws JAXBException, PropertyException, IOException {
		createPhonebookMarshaller().marshal(phonebook, writer);
		return writer;

	}

	private static <T extends OutputStream> T writeXml(Phonebooks phonebooks,
			T outputStream) throws JAXBException, PropertyException,
			IOException {
		createPhonebooksMarshaller().marshal(phonebooks, outputStream);
		return outputStream;

	}

	private static <T extends Writer> T writeXml(Phonebooks phonebooks, T writer)
			throws JAXBException, PropertyException, IOException {
		createPhonebooksMarshaller().marshal(phonebooks, writer);
		return writer;

	}

	private static Phonebook createPhonebook(Iterable<Contact> contacts) {
		Phonebook phonebook = new Phonebook();
		phonebook.getContact().addAll(
				Lists.newArrayList(Iterables.transform(contacts,
						Converter.toMapping)));
		return phonebook;
	}

	private static Phonebooks createPhonebooks(Iterable<Phonebook> phonebooks) {
		Phonebooks result = new Phonebooks();
		Iterables.addAll(result.getPhonebook(), phonebooks);
		return result;
	}

	private static Marshaller createPhonebookMarshaller() throws JAXBException,
			PropertyException {
		return configure(JAXBContext.newInstance(Phonebook.class)
				.createMarshaller());
	}

	private static Marshaller createPhonebooksMarshaller()
			throws JAXBException, PropertyException {
		return configure(JAXBContext.newInstance(Phonebooks.class)
				.createMarshaller());
	}

	private static Marshaller configure(Marshaller marshaller)
			throws PropertyException {
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		return marshaller;
	}

	// ------------------------------------------------------------------------------

	public static final List<Contact> readPhonebooks(InputStream is)
			throws JAXBException {
		return Lists.transform(loadPhonebooks(is), Converter.fromMapping);
	}

	public static final List<Contact> readPhonebooks(Reader reader)
			throws JAXBException {
		return Lists.transform(loadPhonebooks(reader), Converter.fromMapping);
	}

	// ------------------------------------------------------------------------------

	private static List<com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact> loadPhonebooks(
			InputStream is) throws JAXBException {
		return extractContacts(toPhonebooks(createUnmarshaller(Phonebook.class,
				Phonebooks.class).unmarshal(is)));
	}

	private static List<com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact> loadPhonebooks(
			Reader reader) throws JAXBException {
		return extractContacts(toPhonebooks(createUnmarshaller(Phonebook.class,
				Phonebooks.class).unmarshal(reader)));
	}

	private static Phonebooks toPhonebooks(@Nonnull Object object) {
		if (object instanceof Phonebooks) {
			return (Phonebooks) object;
		} else if (object instanceof Phonebook) {
			Phonebook phonebook = (Phonebook) object;
			Phonebooks phonebooks = new Phonebooks();
			phonebooks.getPhonebook().add(phonebook);
			return phonebooks;
		} else {
			throw new IllegalStateException(checkNotNull(object, "object")
					.getClass() + " not supported");
		}
	}

	private static List<com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact> extractContacts(
			Phonebooks phonebooks) {
		List<com.github.pfichtner.gdata2fritz.fritzbox.jaxb.Contact> result = Lists
				.newArrayList();
		for (Phonebook phonebook : phonebooks.getPhonebook()) {
			result.addAll(phonebook.getContact());
		}
		return result;
	}

	private static Unmarshaller createUnmarshaller(Class<?>... classes)
			throws JAXBException {
		return JAXBContext.newInstance(classes).createUnmarshaller();
	}

}
