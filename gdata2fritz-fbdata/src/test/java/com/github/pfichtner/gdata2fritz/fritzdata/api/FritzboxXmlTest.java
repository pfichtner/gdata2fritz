package com.github.pfichtner.gdata2fritz.fritzdata.api;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;

import lombok.Cleanup;

import org.junit.Test;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gc2fritz.data.api.util.ContactBuilder;
import com.google.common.collect.ImmutableList;

public class FritzboxXmlTest {

	@Test
	public void testPhonebook() throws PropertyException, JAXBException,
			IOException {
		List<Contact> contacts = createContactsWithEmail();

		@Cleanup
		StringReader reader = new StringReader(writePhonebook(contacts)
				.getBuffer().toString());

		// since fritzbox xsd does not include email addresses, we will receive
		// the objects without email addresses
		assertEquals(createContactsWithoutEmail(),
				FritzboxXml.readPhonebooks(reader));
	}

	@Test
	public void testPhonebooks() throws PropertyException, JAXBException,
			IOException {
		List<Contact> contacts = createContactsWithEmail();

		@Cleanup
		StringReader reader = new StringReader(writePhonebooks(contacts)
				.getBuffer().toString());

		// since fritzbox xsd does not include email addresses, we will receive
		// the objects without email addresses
		assertEquals(createContactsWithoutEmail(),
				FritzboxXml.readPhonebooks(reader));
	}

	private static List<Contact> createContactsWithEmail() {
		return ImmutableList.of(new ContactBuilder("Peter", "Fichtner")
				.addPhoneNumber("123").addEmail("a@b").build(),
				new ContactBuilder("Foo", "Bar").addPhoneNumber("1")
						.addPhoneNumber("2").build());
	}

	private static ImmutableList<Contact> createContactsWithoutEmail() {
		return ImmutableList.of(new ContactBuilder("Peter", "Fichtner")
				.addPhoneNumber("123").build(),
				new ContactBuilder("Foo", "Bar").addPhoneNumber("1")
						.addPhoneNumber("2").build());
	}

	private StringWriter writePhonebook(List<Contact> contacts)
			throws JAXBException, PropertyException, IOException {
		@Cleanup
		StringWriter writer = new StringWriter();
		return FritzboxXml.writePhonebook(contacts, writer);
	}

	private StringWriter writePhonebooks(List<Contact> contacts)
			throws JAXBException, PropertyException, IOException {
		@Cleanup
		StringWriter writer = new StringWriter();
		return FritzboxXml.writePhonebooks(contacts, writer);
	}

}
