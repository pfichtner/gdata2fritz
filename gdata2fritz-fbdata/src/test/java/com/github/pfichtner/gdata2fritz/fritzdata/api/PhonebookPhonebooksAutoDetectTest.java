package com.github.pfichtner.gdata2fritz.fritzdata.api;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;

import lombok.Cleanup;

import org.junit.Test;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gc2fritz.data.api.util.ContactBuilder;
import com.google.common.collect.ImmutableList;

public class PhonebookPhonebooksAutoDetectTest {

	@Test
	public void testX() throws PropertyException, JAXBException, IOException {
		List<Contact> contacts = ImmutableList.of(new ContactBuilder("Peter",
				"Fichtner").addPhoneNumber("3").build(), new ContactBuilder(
				"Foo", "Bar").addPhoneNumber("1").addPhoneNumber("2").build());
		assertEquals(contacts, FritzboxXml.readPhonebooks(new StringReader(
				writePhonebook(contacts))));
		assertEquals(contacts, FritzboxXml.readPhonebooks(new StringReader(
				writePhonebooks(contacts))));
	}

	private static String writePhonebook(Iterable<Contact> contacts)
			throws JAXBException, PropertyException, IOException {
		@Cleanup
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		FritzboxXml.writePhonebook(contacts, outputStream);
		return outputStream.toString();
	}

	private static String writePhonebooks(Iterable<Contact> contacts)
			throws JAXBException, PropertyException, IOException {
		@Cleanup
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		FritzboxXml.writePhonebooks(contacts, outputStream);
		return outputStream.toString();
	}

}
