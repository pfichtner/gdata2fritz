package com.github.pfichtner.gdata2fritz.main;

import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_HOST;
import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_PHONEBOOK_ID;
import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_USERNAME;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_HOST;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_PASSWORD;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_PHONEBOOK_ID;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_USERNAME;
import static com.github.pfichtner.gc2fritz.main.Defaults.DEFAULT_COUNTRYCODE;
import static com.github.pfichtner.gc2fritz.main.LongOpts.LONGOPT_COUNTRYCODE;
import static com.github.pfichtner.gc2fritz.main.LongOpts.LONGOPT_REFRESHTOKEN;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gc2fritz.fbcom.api.FritzBoxConnection;
import com.github.pfichtner.gc2fritz.fbcom.impl.AutoDetectFritzBoxConnection;
import com.github.pfichtner.gc2fritz.fbcom.util.PhonebookIds;
import com.github.pfichtner.gc2fritz.main.MergeHelper;
import com.github.pfichtner.gc2fritz.util.Connection;
import com.github.pfichtner.gc2fritz.util.Proto;
import com.github.pfichtner.gdata2fritz.fritzdata.api.FritzboxXml;
import com.google.common.collect.Iterables;

/**
 * Merges different sources (phonebooks) and uploads the merged phonebooks to a
 * FritzBox. All options are passed as command line arguments. Since command
 * line arguments of running processes can be seen by all users this is
 * <b>not</b> the preferred way.
 * 
 * @see GContacts2FritzConfig
 * 
 * @author Peter Fichtner
 */
@Slf4j
public class GContacts2Fritz {

	// -- google (download) ------------------------------------------------

	@Option(name = "-rt", aliases = "--" + LONGOPT_REFRESHTOKEN, multiValued = true, required = true, usage = "google oauth2 refreshtoken(s)")
	private List<String> refreshToken;

	@Option(name = "-cc", aliases = "--" + LONGOPT_COUNTRYCODE, usage = "countrycode to use")
	private String countrycode = DEFAULT_COUNTRYCODE;

	// -- fritzbox (upload) ------------------------------------------------

	@Option(name = "-h", aliases = "--" + LONGOPT_HOST, usage = "URL of the fritzbox")
	private String host = DEFAULT_HOST;

	@Option(name = "-p", aliases = "--" + LONGOPT_PASSWORD, required = true, usage = "password of the fritzbox")
	private String password;

	@Option(name = "-u", aliases = "--" + LONGOPT_USERNAME, usage = "username of the fritzbox")
	private String username = DEFAULT_USERNAME;

	@Option(name = "-i", aliases = "--" + LONGOPT_PHONEBOOK_ID, usage = "phonebook id on the fritzbox")
	private String phoneBookId = String.valueOf(DEFAULT_PHONEBOOK_ID);

	public static void main(String[] args) throws Exception {
		new GContacts2Fritz().run(args);
	}

	private void run(String[] args) throws Exception {
		CmdLineParser cmdLineParser = new CmdLineParser(this);
		try {
			cmdLineParser.parseArgument(args);
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			cmdLineParser.printUsage(System.err);
			return;
		}

		Iterable<Contact> merged = new MergeHelper().countrycode(
				this.countrycode).merge(this.refreshToken);
		byte[] bytes = toBytes(merged);

		@Cleanup
		FritzBoxConnection fbConnect = new AutoDetectFritzBoxConnection(
				new Connection(this.host, Proto.HTTP));
		fbConnect.login(this.username, this.password);

		Integer count = Integer.valueOf(Iterables.size(merged));
		log.info("Uploading {} phonebook contacts", count);
		@Cleanup
		InputStream is = new ByteArrayInputStream(bytes);

		try {
			fbConnect.uploadPhoneBook(is, PhonebookIds
					.resolvePhonebookIdIfNeeded(fbConnect, this.phoneBookId));
			log.info("Uploaded {} phonebook contacts", count);
		} catch (Exception e) {
			log.error("Error uploading {} phonebook contacts", count);
		}
	}

	private static byte[] toBytes(Iterable<Contact> contacts)
			throws JAXBException, PropertyException, IOException {
		@Cleanup
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FritzboxXml.writePhonebook(contacts, baos);
		baos.close();
		return baos.toByteArray();
	}

}
