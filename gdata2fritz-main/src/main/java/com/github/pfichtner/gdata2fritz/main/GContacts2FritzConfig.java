package com.github.pfichtner.gdata2fritz.main;

import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_HOST;
import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_PHONEBOOK_ID;
import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_USERNAME;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_HOST;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_PASSWORD;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_PHONEBOOK_ID;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_USERNAME;
import static com.github.pfichtner.gc2fritz.main.Defaults.DEFAULT_COUNTRYCODE;
import static com.github.pfichtner.gc2fritz.main.LongOpts.LONGOPT_COUNTRYCODE;
import static com.github.pfichtner.gc2fritz.util.Normalizers.retainFrom;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Maps.filterKeys;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gc2fritz.data.api.PhoneNumber;
import com.github.pfichtner.gc2fritz.fbcom.api.FritzBoxConnection;
import com.github.pfichtner.gc2fritz.fbcom.impl.AutoDetectFritzBoxConnection;
import com.github.pfichtner.gc2fritz.fbcom.main.Defaults;
import com.github.pfichtner.gc2fritz.fbcom.util.PhonebookIds;
import com.github.pfichtner.gc2fritz.merge.DefaultContactMerger;
import com.github.pfichtner.gc2fritz.util.Connection;
import com.github.pfichtner.gc2fritz.util.CountryCodeRemover;
import com.github.pfichtner.gc2fritz.util.Proto;
import com.github.pfichtner.gc2fritz.util.TrackingMap;
import com.github.pfichtner.gdata2fritz.fritzdata.api.FritzboxXml;
import com.github.pfichtner.gdata2fritz.gdata.api.GoogleService;
import com.github.pfichtner.gdata2fritz.gdata.api.GoogleServiceOAuth2;
import com.google.api.client.repackaged.com.google.common.base.Joiner;
import com.google.common.base.CharMatcher;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;

/**
 * Merges different sources (phonebooks) and uploads the merged phonebooks to a
 * FritzBox. All options have to be defined in the passed config file.
 * 
 * @author Peter Fichtner
 */
@Slf4j
public class GContacts2FritzConfig {

	/**
	 * If entries are not separated by {@value #DEFAULT_TRICMHAR} a differing
	 * trimchar can be defined
	 */
	private static final String KEY_TRIMCHAR = "trimchar";

	private static final String DEFAULT_TRICMHAR = ":";

	private static final String KEY_PHONEBOOK_PREFIX = "phonebook.";

	private static final ImmutableMap<String, Function<List<String>, List<Contact>>> services = ImmutableMap
			.of("google-oauth2", googleOauth2(), "google-credentials",
					googleCredentials(), "phonebook", phonebook(),
					"other-fritzbox", otherFritzbox());

	private static Function<List<String>, List<Contact>> googleOauth2() {
		return new Function<List<String>, List<Contact>>() {
			public List<Contact> apply(List<String> split) {
				try {
					checkArgs(split, "type", "refreshtoken");
					return new GoogleServiceOAuth2(split.get(1)).listContacts();
				} catch (Exception e) {
					throw Throwables.propagate(e);
				}
			};

		};
	}

	private static Function<List<String>, List<Contact>> googleCredentials() {
		return new Function<List<String>, List<Contact>>() {
			public List<Contact> apply(List<String> split) {
				try {
					checkArgs(split, "type", "user", "password");
					return new GoogleService(split.get(1), split.get(2)
							.toCharArray()).listContacts();
				} catch (Exception e) {
					throw Throwables.propagate(e);
				}
			};

		};
	}

	private static Function<List<String>, List<Contact>> phonebook() {
		return new Function<List<String>, List<Contact>>() {
			public List<Contact> apply(List<String> split) {
				try {
					checkArgs(split, "type", "path");
					@Cleanup
					InputStream is = new FileInputStream(split.get(1));
					return FritzboxXml.readPhonebooks(is);
				} catch (Exception e) {
					throw Throwables.propagate(e);
				}
			};

		};
	}

	private static Function<List<String>, List<Contact>> otherFritzbox() {
		return new Function<List<String>, List<Contact>>() {
			public List<Contact> apply(List<String> split) {
				try {
					checkArgs(split, "type", "url-of-other-fritzbox", "user",
							"password", "phonebookId");
					@Cleanup
					FritzBoxConnection fbConnect = new AutoDetectFritzBoxConnection(
							new Connection(split.get(1), Proto.HTTP));
					fbConnect.login(split.get(2), split.get(3));

					@Cleanup
					ByteArrayOutputStream os = new ByteArrayOutputStream();
					fbConnect
							.downloadPhoneBook(
									os,
									split.get(4).isEmpty() ? Defaults.DEFAULT_PHONEBOOK_ID
											: Integer.parseInt(split.get(4)));
					os.close();
					@Cleanup
					ByteArrayInputStream is = new ByteArrayInputStream(
							os.toByteArray());
					return FritzboxXml.readPhonebooks(is);
				} catch (Exception e) {
					throw Throwables.propagate(e);
				}
			}

		};
	}

	// --------------------------------------------------------------------

	private static void checkArgs(List<String> split, String... names) {
		checkState(
				split.size() == names.length,
				"Line must hold exactly %s elements ("
						+ Joiner.on(':').join(names) + ")",
				Integer.valueOf(names.length));
	}

	// --------------------------------------------------------------------

	@Option(name = "-c", required = true, aliases = "--config", usage = "config file to read")
	private File config;

	public static void main(String[] args) throws Exception {
		new GContacts2FritzConfig().run(args);
	}

	private void run(String[] args) throws Exception {
		CmdLineParser cmdLineParser = new CmdLineParser(this);
		try {
			cmdLineParser.parseArgument(args);
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			cmdLineParser.printUsage(System.err);
			return;
		}

		checkState(this.config.exists(), "%s does not exist", this.config);
		checkState(this.config.canRead(), "%s is not readable", this.config);

		TrackingMap<String, String> configMap = TrackingMap.of(Maps
				.newHashMap(Maps.fromProperties(loadProperties(this.config))));

		Map<String, String> phonebooks = extractPhonebooksAndPurge(configMap);
		checkState(!phonebooks.isEmpty(), "No entries matching \"%s\" found",
				KEY_PHONEBOOK_PREFIX);

		Splitter splitter = Splitter.on(
				configMap.getOptional(KEY_TRIMCHAR).or(
						String.valueOf(DEFAULT_TRICMHAR))).trimResults();

		String countryCode = configMap.getOptional(LONGOPT_COUNTRYCODE).or(
				DEFAULT_COUNTRYCODE);

		DefaultContactMerger merger = new DefaultContactMerger();
		Set<Contact> merged = null;

		Ordering<String> ordering = Ordering.natural().onResultOf(
				extractIntValue(KEY_PHONEBOOK_PREFIX));
		for (String line : ImmutableSortedMap.copyOf(phonebooks, ordering)
				.values()) {
			List<String> split = Lists.newArrayList(splitter.split(line));
			checkState(split.size() >= 2,
					"%s must contain at least 2 entries (got %s)", line, split);
			Iterable<Contact> next = normalizeNumbers(
					normalizeCountryCodes(findService(split.get(0))
							.apply(split), countryCode),
					retainFrom(CharMatcher.DIGIT));
			merged = merged == null ? Sets.newHashSet(next) : merger.merge(
					merged, next);
		}

		byte[] bytes = toBytes(merged);

		Connection connection = new Connection(configMap.getOptional(
				LONGOPT_HOST).or(DEFAULT_HOST), Proto.HTTP);
		log.info("Connecting to {}", connection);

		@Cleanup
		FritzBoxConnection fbConnect = new AutoDetectFritzBoxConnection(
				connection);
		fbConnect.login(
				configMap.getOptional(LONGOPT_USERNAME).or(DEFAULT_USERNAME),
				configMap.getChecked(LONGOPT_PASSWORD));

		Integer count = Integer.valueOf(Iterables.size(merged));
		log.info("Uploading {} phonebook contacts", count);
		@Cleanup
		InputStream is = new ByteArrayInputStream(bytes);
		try {
			String phoneBookValue = configMap.getOptional(LONGOPT_PHONEBOOK_ID)
					.or(String.valueOf(DEFAULT_PHONEBOOK_ID));
			fbConnect.uploadPhoneBook(is, PhonebookIds
					.resolvePhonebookIdIfNeeded(fbConnect, phoneBookValue));
			log.info("Uploaded {} phonebook contacts", count);
		} catch (Exception e) {
			log.error("Error uploading {} phonebook contacts", count, e);
		}

		Collection<String> unretrievedKeys = configMap.getUnretrievedKeys();
		if (!unretrievedKeys.isEmpty()) {
			log.warn("Unsupported keys in map: {}", unretrievedKeys);
		}
	}

	private static Map<String, String> extractPhonebooksAndPurge(
			Map<String, String> map) {
		Predicate<String> startsWithPhonebook = startsWith(KEY_PHONEBOOK_PREFIX);
		Map<String, String> phonebooks = Maps.newHashMap(filterKeys(map,
				startsWithPhonebook));
		map.keySet().removeAll(phonebooks.keySet());
		return phonebooks;
	}

	private static Function<String, Integer> extractIntValue(final String prefix) {
		return new Function<String, Integer>() {
			private final Integer noMatch = Integer.valueOf(-1);

			@Override
			public Integer apply(String string) {
				int beginIndex = prefix.length();
				Integer value = beginIndex >= string.length() ? null : Ints
						.tryParse(string.substring(beginIndex));
				return value == null ? this.noMatch : value;
			}
		};
	}

	private Function<List<String>, List<Contact>> findService(String type) {
		return checkNotNull(services.get(type),
				"%s not supported, please use one of %s", type,
				services.keySet());
	}

	private static byte[] toBytes(Iterable<Contact> contacts)
			throws JAXBException, PropertyException, IOException {
		@Cleanup
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FritzboxXml.writePhonebook(contacts, baos);
		baos.close();
		return baos.toByteArray();
	}

	private static Predicate<String> startsWith(final String string) {
		return new Predicate<String>() {
			@Override
			public boolean apply(@Nullable String key) {
				return key.startsWith(string);
			}
		};
	}

	private static Properties loadProperties(File file) throws IOException {
		@Cleanup
		FileInputStream is = new FileInputStream(file);
		Properties properties = new Properties();
		properties.load(is);
		return properties;
	}

	// --------------------------------------------------------------------

	private Iterable<Contact> normalizeCountryCodes(Iterable<Contact> contacts,
			String countrycode) {
		Function<String, String> normalizer = CountryCodeRemover
				.forCC(countrycode);
		return normalizeNumbers(contacts, normalizer);
	}

	private Iterable<Contact> normalizeNumbers(Iterable<Contact> contacts,
			Function<String, String> normalizer) {
		return transform(contacts, normalizeNumbersFunction(normalizer));
	}

	/**
	 * Creates a Function that applies the passed Function to all the Contact's
	 * phonenumbers.
	 * 
	 * @param normalizer
	 *            the normalizer to apply the passed function to
	 * @return a Function that applies the passed Function to all the Contact's
	 *         phonenumbers.
	 */
	private Function<Contact, Contact> normalizeNumbersFunction(
			final Function<String, String> normalizer) {
		return new Function<Contact, Contact>() {
			@Override
			public Contact apply(Contact contact) {
				for (PhoneNumber number : contact.getPhoneNumbers()) {
					number.setValue(normalizer.apply(number.getValue()));
				}
				return contact;
			}
		};
	}

}
