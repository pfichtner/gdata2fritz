package com.github.pfichtner.gdata2fritz.gdata.main;

import static com.github.pfichtner.gdata2fritz.gdata.api.GoogleServiceOAuth2.BASE;
import static com.github.pfichtner.gdata2fritz.gdata.api.GoogleServiceOAuth2.HTTP_TRANSPORT;
import static com.github.pfichtner.gdata2fritz.gdata.api.GoogleServiceOAuth2.JSON_FACTORY;
import static com.github.pfichtner.gdata2fritz.gdata.api.GoogleServiceOAuth2.TOKEN_SERVER_URL;

import java.io.IOException;
import java.util.Scanner;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.ClientParametersAuthentication;
import com.google.api.client.auth.oauth2.TokenResponse;

// http://www.regele.org/tec/google-api-tasks.nutzen.asciidoc.gen.html
public class RequestToken {

	private static final String GOOGLE_OAUTH2_AUTH_URI = "https://accounts.google.com/o/oauth2/auth";

	private static final ClientParametersAuthentication CLIENT_AUTHENTICATION = new ClientParametersAuthentication(
			"845666261507-oqhho5ujc11l3fp1m3ktdcs4iti04guv.apps.googleusercontent.com",
			"u4LgaN36B7pRYodiZWrn8sjm");

	public static void main(String[] args) throws IOException {
		AuthorizationCodeFlow authorizationCodeFlow = createAuthorizationCodeFlow();
		String authorizationUrlString = authorizationCodeFlow
				.newAuthorizationUrl()
				.setRedirectUri("urn:ietf:wg:oauth:2.0:oob").build();
		System.out.println("Please open the URL in your browser "
				+ authorizationUrlString);
		System.out.print("and paste the shown code here: ");
		String token = new Scanner(System.in).nextLine();
		TokenResponse response = authorizationCodeFlow.newTokenRequest(token)
				.setRedirectUri("urn:ietf:wg:oauth:2.0:oob").execute();
		System.out.println("Generated refreshToken: "
				+ response.getRefreshToken());
	}

	private static AuthorizationCodeFlow createAuthorizationCodeFlow() {
		return new AuthorizationCodeFlow.Builder(
				BearerToken.authorizationHeaderAccessMethod(), HTTP_TRANSPORT,
				JSON_FACTORY, TOKEN_SERVER_URL, CLIENT_AUTHENTICATION,
				CLIENT_AUTHENTICATION.getClientId(), GOOGLE_OAUTH2_AUTH_URI)
				.setScopes(BASE).build();
	}

}
