package com.github.pfichtner.gdata2fritz.gdata.api;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class App {

	public static final String name = "pfichtner-gdata2fritz-"
			+ getImplementationVersion();

	private static String getImplementationVersion() {
		String implementationVersion = App.class.getPackage()
				.getImplementationVersion();
		return implementationVersion == null ? "dev" : implementationVersion;
	}

}
