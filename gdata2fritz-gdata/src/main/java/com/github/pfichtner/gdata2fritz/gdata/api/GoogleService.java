package com.github.pfichtner.gdata2fritz.gdata.api;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gdata2fritz.gdata.impl.Converter;
import com.google.common.collect.Lists;
import com.google.gdata.client.Query;
import com.google.gdata.client.contacts.ContactsService;
import com.google.gdata.data.contacts.ContactFeed;
import com.google.gdata.data.contacts.ContactGroupEntry;
import com.google.gdata.data.contacts.ContactGroupFeed;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

public class GoogleService {

	private static final String BASE = "https://www.google.com/m8/feeds/";
	private static final String DEFAULT_FULL = "/default/full";

	private final ContactsService delegate;

	public GoogleService(String user, char[] password)
			throws AuthenticationException {
		this.delegate = new ContactsService(App.name);
		this.delegate.setUserCredentials(user, new String(password));
	}

	public List<Contact> listContacts() throws IOException {
		Query query = new Query(new URL(BASE + "contacts" + DEFAULT_FULL));
		query.setMaxResults(Integer.MAX_VALUE);
		try {
			return Lists.transform(this.delegate
					.query(query, ContactFeed.class).getEntries(),
					Converter.fromMapping);
		} catch (ServiceException e) {
			throw new IOException(e);
		}
	}

	public List<ContactGroupEntry> listGroups() throws ServiceException,
			IOException {
		Query query = new Query(new URL(BASE + "groups" + DEFAULT_FULL));
		query.setMaxResults(Integer.MAX_VALUE);
		return this.delegate.query(query, ContactGroupFeed.class).getEntries();
	}

}
