package com.github.pfichtner.gdata2fritz.gdata.impl;

import static com.google.gdata.model.gd.PhoneNumber.Rel.ASSISTANT;
import static com.google.gdata.model.gd.PhoneNumber.Rel.CALLBACK;
import static com.google.gdata.model.gd.PhoneNumber.Rel.CAR;
import static com.google.gdata.model.gd.PhoneNumber.Rel.FAX;
import static com.google.gdata.model.gd.PhoneNumber.Rel.HOME;
import static com.google.gdata.model.gd.PhoneNumber.Rel.HOME_FAX;
import static com.google.gdata.model.gd.PhoneNumber.Rel.MOBILE;
import static com.google.gdata.model.gd.PhoneNumber.Rel.PAGER;
import static com.google.gdata.model.gd.PhoneNumber.Rel.RADIO;
import static com.google.gdata.model.gd.PhoneNumber.Rel.WORK;
import static com.google.gdata.model.gd.PhoneNumber.Rel.WORK_FAX;
import static com.google.gdata.model.gd.PhoneNumber.Rel.WORK_MOBILE;
import static com.google.gdata.model.gd.PhoneNumber.Rel.WORK_PAGER;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gc2fritz.data.api.Person;
import com.github.pfichtner.gc2fritz.data.api.PhoneNumber;
import com.github.pfichtner.gc2fritz.data.api.PhoneNumberType;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.gdata.data.contacts.ContactEntry;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class Converter {

	private static final BiMap<String, PhoneNumberType> map = ImmutableBiMap
			.<String, PhoneNumberType> builder()
			.put(HOME, PhoneNumberType.HOME)
			.put(MOBILE, PhoneNumberType.MOBILE)
			.put(WORK, PhoneNumberType.WORK)
			.put(WORK_MOBILE, PhoneNumberType.WORK_MOBILE)
			.put(CALLBACK, PhoneNumberType.CALLBACK)
			.put(ASSISTANT, PhoneNumberType.ASSISTANT)
			.put(FAX, PhoneNumberType.FAX)
			.put(HOME_FAX, PhoneNumberType.HOME_FAX)
			.put(WORK_FAX, PhoneNumberType.WORK_FAX)
			.put(PAGER, PhoneNumberType.PAGER)
			.put(WORK_PAGER, PhoneNumberType.WORK_PAGER)
			.put(CAR, PhoneNumberType.CAR).put(RADIO, PhoneNumberType.RADIO)
			.build();
	private static final Function<String, PhoneNumberType> mappingA = Functions
			.forMap(map, null);
	// private static final Function<PhoneNumberType, String> mappingB =
	// Functions
	// .forMap(map.inverse(), null);

	public static final Function<ContactEntry, Contact> fromMapping = new Function<ContactEntry, Contact>() {
		@Override
		public Contact apply(ContactEntry contactEntry) {
			return fromGoogle(contactEntry);
		}
	};

	private static Contact fromGoogle(ContactEntry entry) {
		Contact result = new Contact();
		Person person = new Person(extractName(entry));
		result.setPerson(person);
		for (com.google.gdata.data.extensions.PhoneNumber phoneNumber : entry
				.getPhoneNumbers()) {
			result.addPhoneNumber(new PhoneNumber(mappingA.apply(phoneNumber
					.getRel()), phoneNumber.getPhoneNumber()));
		}
		return result;
	}

	private static String extractName(ContactEntry entry) {
		return entry.getName() == null || entry.getName().getFullName() == null ? null
				: entry.getName().getFullName().getValue();
	}

}
