package com.github.pfichtner.gdata2fritz.gdata.api;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gdata2fritz.gdata.impl.Converter;
import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.ClientParametersAuthentication;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.RefreshTokenRequest;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import com.google.gdata.client.Query;
import com.google.gdata.client.contacts.ContactsService;
import com.google.gdata.data.contacts.ContactFeed;
import com.google.gdata.data.contacts.ContactGroupEntry;
import com.google.gdata.data.contacts.ContactGroupFeed;
import com.google.gdata.util.ServiceException;

public class GoogleServiceOAuth2 {

	private static final String DEFAULT_FULL = "/default/full";

	public static final String BASE = "https://www.google.com/m8/feeds/";

	/** Global instance of the HTTP transport */
	public static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

	public static final JsonFactory JSON_FACTORY = new JacksonFactory();

	// ----------------------------------------------------------------------------

	public static final GenericUrl TOKEN_SERVER_URL = new GenericUrl(
			"https://accounts.google.com/o/oauth2/token");

	public static final ClientParametersAuthentication CLIENT_AUTHENTICATION = new ClientParametersAuthentication(
			"845666261507-oqhho5ujc11l3fp1m3ktdcs4iti04guv.apps.googleusercontent.com",
			"u4LgaN36B7pRYodiZWrn8sjm");

	// ----------------------------------------------------------------------------

	private final ContactsService delegate;

	public GoogleServiceOAuth2(String refreshToken) {
		this.delegate = new ContactsService(App.name);
		try {
			this.delegate.setOAuth2Credentials(createCredentials(refreshToken));
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	public List<Contact> listContacts() throws IOException {
		Query query = new Query(new URL(BASE + "contacts" + DEFAULT_FULL));
		query.setMaxResults(Integer.MAX_VALUE);
		try {
			return Lists.transform(this.delegate
					.query(query, ContactFeed.class).getEntries(),
					Converter.fromMapping);
		} catch (ServiceException e) {
			throw new IOException(e);
		}
	}

	public List<ContactGroupEntry> listGroups() throws ServiceException,
			IOException {
		Query query = new Query(new URL(BASE + "groups" + DEFAULT_FULL));
		query.setMaxResults(Integer.MAX_VALUE);
		return this.delegate.query(query, ContactGroupFeed.class).getEntries();
	}

	// -----------------------------------------------------------------------------

	private static Credential createCredentials(String refreshToken)
			throws IOException, URISyntaxException {
		return new Credential(BearerToken.authorizationHeaderAccessMethod())
				.setFromTokenResponse(createToken(refreshToken));
	}

	private static TokenResponse createToken(String refreshToken)
			throws IOException, URISyntaxException {
		return new RefreshTokenRequest(HTTP_TRANSPORT, JSON_FACTORY,
				TOKEN_SERVER_URL, refreshToken).setClientAuthentication(
				CLIENT_AUTHENTICATION).execute();
	}

}
