package com.github.pfichtner.gdata2fritz.gdata.main;

import static com.github.pfichtner.gdata2fritz.gdata.api.GoogleServiceOAuth2.BASE;
import static com.github.pfichtner.gdata2fritz.gdata.api.GoogleServiceOAuth2.CLIENT_AUTHENTICATION;
import static com.github.pfichtner.gdata2fritz.gdata.api.GoogleServiceOAuth2.HTTP_TRANSPORT;
import static com.github.pfichtner.gdata2fritz.gdata.api.GoogleServiceOAuth2.JSON_FACTORY;
import static com.google.common.base.Preconditions.checkState;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.util.Key;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;

// http://www.regele.org/tec/google-api-tasks.nutzen.asciidoc.gen.html
public class RequestTokenLimitedDevice {

	private static class DefaultTimeoutWatcher implements TimeoutWatcher {

		private final long millis;

		public DefaultTimeoutWatcher(long timeout, TimeUnit unit) {
			this.millis = MILLISECONDS.convert(timeout, unit);
		}

		@Override
		public void checkTimeout(long l, TimeUnit timeUnit) {
			checkState(MILLISECONDS.convert(l, timeUnit) <= this.millis,
					"Timeout, waited more than %s millis",
					Long.valueOf(this.millis));
		}

	}

	private interface TimeoutWatcher {
		TimeoutWatcher NULL = new TimeoutWatcher() {
			@Override
			public void checkTimeout(long l, TimeUnit seconds) {
				// do nothing, no timeout
			}
		};

		void checkTimeout(long l, TimeUnit seconds);
	}

	@Data
	@EqualsAndHashCode(callSuper = true)
	public static class DeviceCodeTokenResponse extends TokenResponse {

		@Key("interval")
		private int interval;

		@Key("device_code")
		private String deviceCode;

		@Key("verification_url")
		private String verificationUrl;

		@Key("user_code")
		private String userCode;

	}

	@Data
	@EqualsAndHashCode(callSuper = true)
	public static class TokenResponseWithError extends TokenResponse {

		@Key("error")
		private String error;

	}

	public static void main(String[] args) throws IOException,
			InterruptedException {
		DeviceCodeTokenResponse deviceCodeResponse = getDeviceToken();
		System.out.println("Please open the url "
				+ deviceCodeResponse.getVerificationUrl()
				+ " in your browser, enter the device code "
				+ deviceCodeResponse.getUserCode()
				+ " and authenticate the app");

		TokenResponse response = waitForAuth(deviceCodeResponse);
		System.out.println("Generated refreshToken: "
				+ response.getRefreshToken());
	}

	private static DeviceCodeTokenResponse getDeviceToken() throws IOException {
		Map<String, String> data = ImmutableMap.of("client_id",
				CLIENT_AUTHENTICATION.getClientId(), "scope", BASE);
		return execute("https://accounts.google.com/o/oauth2/device/code", data)
				.parseAs(DeviceCodeTokenResponse.class);
	}

	@Nonnull
	private static HttpResponse execute(String url, Map<String, String> data)
			throws IOException {
		return HTTP_TRANSPORT
				.createRequestFactory()
				.buildPostRequest(new GenericUrl(url),
						new UrlEncodedContent(data))
				.setParser(new JsonObjectParser(JSON_FACTORY)).execute();
	}

	private static TokenResponse waitForAuth(DeviceCodeTokenResponse userToken)
			throws InterruptedException, IOException {
		return waitForAuth(userToken, 0, null);
	}

	@Nonnull
	private static TokenResponse waitForAuth(DeviceCodeTokenResponse userToken,
			long timeout, TimeUnit unit) throws InterruptedException,
			IOException {
		return waitForAuth(
				userToken,
				createTimeoutWatcher(userToken.getExpiresInSeconds(),
						TimeUnit.SECONDS));
	}

	private static TimeoutWatcher createTimeoutWatcher(Long timeout,
			TimeUnit unit) {
		return hasTimeout(timeout, unit) ? new DefaultTimeoutWatcher(
				timeout.longValue(), unit) : TimeoutWatcher.NULL;
	}

	private static boolean hasTimeout(Long timeout, TimeUnit unit) {
		return unit != null && timeout != null && timeout.longValue() > 0;
	}

	private static TokenResponseWithError waitForAuth(
			DeviceCodeTokenResponse userToken, TimeoutWatcher timeoutWatcher)
			throws InterruptedException, IOException {
		int sleepIntervallSecs = userToken.getInterval();
		Stopwatch stopwatch = new Stopwatch().start();
		while (true) {
			timeoutWatcher.checkTimeout(stopwatch.elapsed(TimeUnit.SECONDS),
					TimeUnit.SECONDS);
			TimeUnit.SECONDS.sleep(sleepIntervallSecs);

			Map<String, String> data = ImmutableMap.of("client_id",
					CLIENT_AUTHENTICATION.getClientId(), "scope", BASE,
					"client_secret", CLIENT_AUTHENTICATION.getClientSecret(),
					"code", userToken.getDeviceCode(), "grant_type",
					"http://oauth.net/grant_type/device/1.0");
			TokenResponseWithError accessToken = execute(
					"https://accounts.google.com/o/oauth2/token", data)
					.parseAs(TokenResponseWithError.class);
			String error = accessToken.getError();
			if (error == null) {
				return accessToken;
			} else if ("slow_down".equals(error)) {
				sleepIntervallSecs++;
			} else {
				checkState("authorization_pending".equals(error), error);
			}
		}
	}

}
