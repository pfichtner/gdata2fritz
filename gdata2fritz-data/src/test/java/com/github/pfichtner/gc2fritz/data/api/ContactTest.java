package com.github.pfichtner.gc2fritz.data.api;

import static com.github.pfichtner.gc2fritz.data.api.PhoneNumberType.HOME;
import static com.github.pfichtner.gc2fritz.data.api.PhoneNumberType.MOBILE;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.github.pfichtner.gc2fritz.data.api.util.ContactBuilder;

public class ContactTest {

	@Test
	public void testEquals() {
		assertThat(createContact(HOME), equalTo(createContact(HOME)));
	}

	@Test
	public void testNotEquals() {
		assertThat(createContact(HOME), not(equalTo(createContact(MOBILE))));
	}

	private static Contact createContact(PhoneNumberType type) {
		return new ContactBuilder("Peter", "Fichtner").addPhoneNumber(type,
				"+49 175 123456").build();
	}

}
