package com.github.pfichtner.gc2fritz.data.api;

import java.util.List;

import lombok.Data;

import com.google.common.collect.Lists;

@Data
public class Contact {

	private String category;
	private Person person;
	private List<PhoneNumber> phoneNumbers = Lists.newArrayList();
	private List<Email> emails = Lists.newArrayList();

	public void addPhoneNumber(PhoneNumber number) {
		this.phoneNumbers.add(number);
	}

	public void addEmail(Email email) {
		this.emails.add(email);
	}

}
