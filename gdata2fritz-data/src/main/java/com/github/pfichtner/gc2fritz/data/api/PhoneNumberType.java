package com.github.pfichtner.gc2fritz.data.api;

public enum PhoneNumberType {
	HOME, MOBILE, WORK, WORK_MOBILE, CALLBACK, ASSISTANT, FAX, HOME_FAX, WORK_FAX, PAGER, WORK_PAGER, CAR, RADIO;
}
