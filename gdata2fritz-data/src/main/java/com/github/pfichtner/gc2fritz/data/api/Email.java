package com.github.pfichtner.gc2fritz.data.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Email {

	private String type;
	private String address;

	public Email(String address) {
		this.address = address;
	}

}
