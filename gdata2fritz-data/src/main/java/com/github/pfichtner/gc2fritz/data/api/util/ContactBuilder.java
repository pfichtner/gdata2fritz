package com.github.pfichtner.gc2fritz.data.api.util;

import static com.github.pfichtner.gc2fritz.data.api.PhoneNumberType.HOME;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gc2fritz.data.api.Email;
import com.github.pfichtner.gc2fritz.data.api.Person;
import com.github.pfichtner.gc2fritz.data.api.PhoneNumber;
import com.github.pfichtner.gc2fritz.data.api.PhoneNumberType;

public class ContactBuilder {

	private final Contact contact;

	public ContactBuilder(String givenName, String familyName) {
		this.contact = new Contact();
		Person person = new Person(givenName + " " + familyName);
		this.contact.setPerson(person);
	}

	public ContactBuilder addPhoneNumber(String number) {
		return addPhoneNumber(HOME, number);
	}

	public ContactBuilder addPhoneNumber(PhoneNumberType type, String number) {
		return addPhoneNumber(new PhoneNumber(type, number));
	}

	private ContactBuilder addPhoneNumber(PhoneNumber number) {
		this.contact.addPhoneNumber(number);
		return this;
	}

	public ContactBuilder addEmail(String email) {
		this.contact.addEmail(new Email(email));
		return this;
	}

	public Contact build() {
		return this.contact;
	}

}