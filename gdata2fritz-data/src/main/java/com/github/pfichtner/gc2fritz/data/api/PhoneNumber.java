package com.github.pfichtner.gc2fritz.data.api;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PhoneNumber {

	private PhoneNumberType type;
	private String value;
	private String prio;
	private String quickdial;
	private String vanity;

	public PhoneNumber(String value) {
		this.value = value;
	}

	public PhoneNumber(PhoneNumberType type, String value) {
		this(value);
		this.type = type;
	}

}
