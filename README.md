# gdata2fritz [![Build Status](https://travis-ci.org/pfichtner/gdata2fritz.svg?branch=master)](https://travis-ci.org/pfichtner/gdata2fritz)

Synchronize your google contacts (multiple accounts are supported) to your AVM Fritz!Box [TM]. 

You can download the latest version of [ gdata2fritz.jar ](https://bintray.com/pfichtner/maven/gdata2fritz/_latestVersion) from bintray. 


## Usage

```shell
java -jar gdata2fritz-0.0.5.jar -c <path/to/configfile>;
```

### Required entries in the config are 
* phonebook.X=type:..., where X is an Integer value

  Multiple entries will be merged in the order of the key's Integer value

#### Supported types are: 
type              |parameters                                                   |description
------------------|-------------------------------------------------------------|----------------------------------------------------------
google-oauth2     |refreshtoken                                            |import from google account using oauth2 refreshToken (for generating refreshTokens read below)
google-credentials|user:password                                           |import from google account using credentials
phonebook         |path                                                    |import a "phonebook" or "phonebooks" xml file
other-fritzbox    |other-fritzbox-name-or-ip:port:user:password:phonebookId|import directly from other fritz box

* password=secret: the FritzBox password

### Optional entries are: 
* countrycode: if you leave outside germany you should pass "+xx" or "00xx" (default is +49)
* hostname": hostname of the fritz box (default is fritz.box)
* port: port of the fritz box (default is 80 or 443, depending on usessl)
* phoneBookId: phonebook index (or name) to use on the fritz.box (default is 0)
* usessl: whether to use SSL or not when communicating with the fritz box

#### Generating google refreshTokens: 
There are two main-classes for generating refreshTokens: *RequestToken* and *RequestTokenLimitedDevice* 
You can execute them by callling `java -cp gdata2fritz-0.0.5.jar com.github.pfichtner.gdata2fritz.gdata.main.RequestToken` or `java -cp gdata2fritz-0.0.5.jar com.github.pfichtner.gdata2fritz.gdata.main.RequestTokenLimitedDevice` 
  
You should use *RequestToken* (will generate a long URL) if you can paste the URL into your browser otherwise use *RequestTokenLimitedDevice* (will generate a short URL and a device-id). 

#### Why a config file instead of parameters? 
Because parameters of running processes can be seen by any user on that machine. You don't want to expose your Fritz!Box's or your google accounts' passwords nor the refreshTokens! 
That's the reason why you should protect the config file from being world readable! 

## License

Copyright 2013-2014 Peter Fichtner - Released under the [Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0.html)
