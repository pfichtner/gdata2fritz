package com.github.pfichtner.gc2fritz.util;

import static com.google.common.base.Functions.compose;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import com.google.common.base.CharMatcher;
import com.google.common.base.Function;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class Normalizers {

	public static Function<String, String> unifyNumber = unifyNumber();

	private static Function<String, String> unifyNumber() {
		// let's consider 3 types of numbers: +49 721... 0721... 0049721... +01
		// 123... 0001123...
		Function<String, String> f1 = CountryCodePlusToZeroZero.INSTANCE;
		// now they are: 0049 721... 0721... 0049721... 0001 123... 0001123...
		Function<String, String> f2 = retainFrom(CharMatcher.DIGIT);
		// now they are: 0049721... 0721... 0049721... 0001123... 0001123...
		Function<String, String> f3 = CountryCodeAdder.forCC("0049");
		// now they are: 0049721... 0049721... 0049721... 0001123... 0001123...
		return compose(compose(f3, f2), f1);
	}

	private static final Function<?, ?> NOOP = new Function<Object, Object>() {
		@Override
		public Object apply(Object input) {
			return input;
		}
	};

	@SuppressWarnings("unchecked")
	public static <T> Function<T, T> noop() {
		return (Function<T, T>) Normalizers.NOOP;
	}

	public static Function<String, String> retainFrom(
			final CharMatcher charMatcher) {
		return new Function<String, String>() {
			@Override
			public String apply(String input) {
				return charMatcher.retainFrom(input);
			}
		};
	}

}
