package com.github.pfichtner.gc2fritz.util;

import lombok.Data;

import com.google.common.base.Function;

/**
 * Adds the passed countrycode to each number if the number starts with a prefix
 * (e.g. <code>0721...</code>). If the number starts already with a countrycode
 * (e.g. <code>0049 721...</code>) the number keeps untouched.
 * 
 * @author Peter Fichtner
 */
@Data
public class CountryCodeAdder implements Function<String, String> {

	private final String countrycode;

	private CountryCodeAdder(String countrycode) {
		this.countrycode = CountryCodePlusToZeroZero.INSTANCE
				.apply(countrycode);
	}

	public static Function<String, String> forCC(String countrycode) {
		return countrycode == null ? Normalizers.<String> noop()
				: new CountryCodeAdder(
						CountryCodePlusToZeroZero.INSTANCE.apply(countrycode));
	}

	@Override
	public String apply(String in) {
		return startsWithPrefix(in) ? this.countrycode + in.substring(1) : in;
	}

	private boolean startsWithPrefix(String in) {
		return in.length() >= 2 && (in.charAt(0) == '0' && in.charAt(1) != '0');
	}

}
