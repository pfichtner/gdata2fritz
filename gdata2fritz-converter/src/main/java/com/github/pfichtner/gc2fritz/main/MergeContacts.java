package com.github.pfichtner.gc2fritz.main;

import static com.github.pfichtner.gc2fritz.main.Defaults.DEFAULT_COUNTRYCODE;
import static com.github.pfichtner.gc2fritz.main.LongOpts.LONGOPT_COUNTRYCODE;
import static com.github.pfichtner.gc2fritz.main.LongOpts.LONGOPT_REFRESHTOKEN;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;

import lombok.extern.slf4j.Slf4j;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gdata2fritz.fritzdata.api.FritzboxXml;

@Slf4j
public class MergeContacts {

	// retrieve all memberships:
	// https://developers.google.com/google-apps/contacts/v3/#retrieving_all_contact_groups
	// then add include/exclude option for contact-groups (--include
	// 0:groupX,1:family)

	@Option(name = "-rt", aliases = "--" + LONGOPT_REFRESHTOKEN, multiValued = true, required = true, usage = "google oauth2 refreshtoken(s)")
	private List<String> refreshToken;

	@Option(name = "-cc", aliases = "--" + LONGOPT_COUNTRYCODE, usage = "countrycode to use")
	private String countrycode = DEFAULT_COUNTRYCODE;

	@Option(name = "-o", aliases = "--out", usage = "file (phonebook.xml) to write")
	private File targetFile;

	public static void main(String[] args) throws CmdLineException,
			IOException, PropertyException, JAXBException {
		new MergeContacts().doMain(args);
	}

	private void doMain(String[] args) throws CmdLineException, IOException,
			PropertyException, JAXBException {
		CmdLineParser cmdLineParser = new CmdLineParser(this);
		try {
			cmdLineParser.parseArgument(args);
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			cmdLineParser.printUsage(System.err);
			return;
		}

		log.info("Merging contacts from {} accounts",
				Integer.valueOf(this.refreshToken.size()));
		Set<Contact> merged = new MergeHelper().countrycode(this.countrycode)
				.merge(this.refreshToken);
		if (this.targetFile == null) {
			FritzboxXml.writePhonebook(merged, new PrintWriter(System.out));
		} else {
			OutputStream target = new FileOutputStream(this.targetFile);
			try {
				FritzboxXml.writePhonebook(merged, target);
				log.info("{} contact(s) written to {}",
						Integer.valueOf(merged.size()), this.targetFile);
			} finally {
				target.close();
			}
		}
	}

}
