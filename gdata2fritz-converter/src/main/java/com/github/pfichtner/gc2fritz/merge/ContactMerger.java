package com.github.pfichtner.gc2fritz.merge;

import java.util.Set;

import com.github.pfichtner.gc2fritz.data.api.Contact;

public interface ContactMerger {

	Set<Contact> merge(Iterable<Contact> entries1, Iterable<Contact> entries2);

}