package com.github.pfichtner.gc2fritz.main;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class Defaults {

	public static final String DEFAULT_COUNTRYCODE = "+49";

}
