package com.github.pfichtner.gc2fritz.util;

import com.google.common.base.Function;
import com.google.common.base.Strings;

/**
 * Replaces phone numbers starting with '+' into phone numbers starting with
 * '00'
 * 
 * @author Peter Fichtner
 */
public class CountryCodePlusToZeroZero implements Function<String, String> {

	/**
	 * Default static instance of {@link CountryCodePlusToZeroZero}.
	 */
	public static final CountryCodePlusToZeroZero INSTANCE = new CountryCodePlusToZeroZero();

	@Override
	public String apply(String phoneNumber) {
		return Strings.isNullOrEmpty(phoneNumber)
				|| phoneNumber.charAt(0) != '+' ? phoneNumber : "00"
				+ phoneNumber.substring(1);
	}

}
