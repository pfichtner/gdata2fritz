package com.github.pfichtner.gc2fritz.main;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class LongOpts {

	public static final String LONGOPT_REFRESHTOKEN = "refreshtoken";

	public static final String LONGOPT_COUNTRYCODE = "countrycode";

}
