package com.github.pfichtner.gc2fritz.merge;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gc2fritz.data.api.Person;
import com.github.pfichtner.gc2fritz.data.api.PhoneNumber;
import com.github.pfichtner.gc2fritz.util.Normalizers;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;

@Slf4j
public class DefaultContactMerger implements ContactMerger {

	private static final Comparator<String> unifyNumber = Ordering.natural()
			.onResultOf(Normalizers.unifyNumber);

	private static final Predicate<Contact> hasAtLeastOnePhoneNumber = new Predicate<Contact>() {
		public boolean apply(Contact entry) {
			return !entry.getPhoneNumbers().isEmpty();
		}
	};

	private static Predicate<Contact> hasRealName = new Predicate<Contact>() {
		public boolean apply(Contact contact) {
			Person person = contact.getPerson();
			return person != null
					&& !Strings.isNullOrEmpty(person.getRealName());
		}
	};

	@Override
	public Set<Contact> merge(Iterable<Contact> contacts1,
			Iterable<Contact> contacts2) {
		Map<String, Contact> byNumber = Maps.newTreeMap(unifyNumber);
		putAll(filter(contacts1), byNumber);
		putAll(filter(contacts2), byNumber);

		Set<Contact> result = Sets.newHashSet();

		// merge by name
		for (Collection<Contact> contacts : LinkedListMultimap
				.create(Multimaps.index(byNumber.values(), realNameFunction()))
				.asMap().values()) {
			Contact first = Iterables.get(contacts, 0);
			result.add(contacts.size() == 1 ? first : merge(first, contacts));
		}

		return result;
	}

	private static Iterable<Contact> filter(Iterable<Contact> entries) {
		return FluentIterable.from(entries).filter(hasAtLeastOnePhoneNumber)
				.filter(hasRealName);
	}

	private static Contact merge(Contact first, Collection<Contact> contacts) {
		log.info("Merging contact \"{}\"", first.getPerson().getRealName());
		for (Contact contact : Iterables.skip(contacts, 1)) {
			if (first != contact) {
				for (com.github.pfichtner.gc2fritz.data.api.PhoneNumber pn : contact
						.getPhoneNumbers()) {
					first.addPhoneNumber(pn);
				}
			}
		}
		return first;
	}

	private static Function<Contact, String> realNameFunction() {
		return new Function<Contact, String>() {
			public String apply(Contact contact) {
				return contact.getPerson().getRealName();
			}
		};
	}

	private static void putAll(Iterable<Contact> contacts,
			Map<String, Contact> byNumber) {
		for (Contact contact : contacts) {
			for (Iterator<PhoneNumber> iterator = contact.getPhoneNumbers()
					.iterator(); iterator.hasNext();) {
				String key = iterator.next().getValue();
				Contact oldValue = byNumber.get(key);
				if (oldValue != null) {
					log.info(
							"Number {} already in map (keeping old value \"{}\", new value would have been \"{}\")",
							key, oldValue.getPerson().getRealName(), contact
									.getPerson().getRealName());
					iterator.remove();
				} else {
					byNumber.put(key, contact);
				}
			}
		}
	}

}
