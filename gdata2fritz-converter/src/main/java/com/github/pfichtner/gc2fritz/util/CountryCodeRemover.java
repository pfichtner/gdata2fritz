package com.github.pfichtner.gc2fritz.util;

import lombok.Data;

import com.google.common.base.CharMatcher;
import com.google.common.base.Function;

/**
 * Translates numbers starting with '+' into numbers starting with '00' and
 * removes leading +XX or 00XX for the passed countrycode from phonenumbers.
 * Numbers starting with other countrycodes are untouched.
 * 
 * @author Peter Fichtner
 */
@Data
public class CountryCodeRemover implements Function<String, String> {

	private final String countrycode;

	private CountryCodeRemover(String countrycode) {
		this.countrycode = CountryCodePlusToZeroZero.INSTANCE
				.apply(countrycode);
	}

	public static Function<String, String> forCC(String countrycode) {
		return countrycode == null ? Normalizers.<String> noop()
				: new CountryCodeRemover(countrycode);
	}

	@Override
	public String apply(String in) {
		String numberWithLeadingZeros = CountryCodePlusToZeroZero.INSTANCE
				.apply(in);
		return startsWithContryCode(numberWithLeadingZeros) ? "0"
				+ CharMatcher.WHITESPACE
						.trimLeadingFrom(cutOfStartingCountryCode(numberWithLeadingZeros))
				: numberWithLeadingZeros;
	}

	private boolean startsWithContryCode(String number) {
		return number.startsWith(this.countrycode);
	}

	private String cutOfStartingCountryCode(String number) {
		return number.substring(this.countrycode.length());
	}

}
