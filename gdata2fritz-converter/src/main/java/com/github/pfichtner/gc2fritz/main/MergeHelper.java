package com.github.pfichtner.gc2fritz.main;

import static com.google.common.collect.Iterables.transform;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.Nullable;

import lombok.Setter;
import lombok.experimental.Accessors;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gc2fritz.data.api.PhoneNumber;
import com.github.pfichtner.gc2fritz.merge.DefaultContactMerger;
import com.github.pfichtner.gc2fritz.util.CountryCodeRemover;
import com.github.pfichtner.gc2fritz.util.Normalizers;
import com.github.pfichtner.gdata2fritz.gdata.api.GoogleServiceOAuth2;
import com.google.common.base.CharMatcher;
import com.google.common.base.Function;
import com.google.common.collect.Sets;

@Setter
@Accessors(fluent = true, chain = true)
public class MergeHelper {

	private String countrycode;

	public Set<Contact> merge(Iterable<String> refreshTokens)
			throws IOException {
		return merge(refreshTokens.iterator());
	}

	private Set<Contact> merge(Iterator<String> refreshTokens)
			throws IOException {
		DefaultContactMerger merger = new DefaultContactMerger();
		Set<Contact> merged = null;

		while (refreshTokens.hasNext()) {
			GoogleServiceOAuth2 googleService = new GoogleServiceOAuth2(
					refreshTokens.next());
			Iterable<Contact> next = normalizeNumber(normalizeCountryCodes(googleService
					.listContacts()));
			merged = merged == null ? Sets.newHashSet(next) : merger.merge(
					merged, next);
		}
		return merged;
	}

	private Iterable<Contact> normalizeNumber(Iterable<Contact> entries) {
		return transform(entries,
				normalizeCountryCodesFunction(Normalizers
						.retainFrom(CharMatcher.DIGIT)));
	}

	private Iterable<Contact> normalizeCountryCodes(Iterable<Contact> entries) {
		return transform(entries,
				normalizeCountryCodesFunction(CountryCodeRemover
						.forCC(this.countrycode)));
	}

	private Function<Contact, Contact> normalizeCountryCodesFunction(
			final Function<String, String> normalizer) {
		return new Function<Contact, Contact>() {
			@Override
			public Contact apply(@Nullable Contact contact) {
				for (PhoneNumber number : contact.getPhoneNumbers()) {
					number.setValue(normalizer.apply(number.getValue()));
				}
				return contact;
			}
		};
	}

}
