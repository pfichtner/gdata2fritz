package com.github.pfichtner.gc2fritz;

import static com.github.pfichtner.gc2fritz.data.api.PhoneNumberType.CAR;
import static com.github.pfichtner.gc2fritz.data.api.PhoneNumberType.HOME;
import static com.github.pfichtner.gc2fritz.data.api.PhoneNumberType.MOBILE;
import static com.github.pfichtner.gc2fritz.data.api.PhoneNumberType.WORK;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Set;

import org.junit.Test;

import com.github.pfichtner.gc2fritz.data.api.Contact;
import com.github.pfichtner.gc2fritz.data.api.util.ContactBuilder;
import com.github.pfichtner.gc2fritz.merge.ContactMerger;
import com.github.pfichtner.gc2fritz.merge.DefaultContactMerger;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;

public class DefaultContactMergerTest {

	private final ContactMerger merger = new DefaultContactMerger();

	@Test
	public void testEmptyLists() {
		assertThat(this.merger.merge(noContacts(), noContacts()), is(empty()));
	}

	private Set<Contact> noContacts() {
		return ImmutableSet.of();
	}

	@Test
	public void testLeftPersonWithoutPhoneNumbers() {
		assertThat(this.merger.merge(ImmutableSet.of(new ContactBuilder(
				"Peter", "Fichtner").build()), noContacts()), is(empty()));
	}

	@Test
	public void testLeftPersonWithOnePhoneNumbers() {
		Contact ce1 = new ContactBuilder("Peter", "Fichtner").addPhoneNumber(
				"+49 175 123456").build();
		Set<Contact> merged = this.merger.merge(ImmutableSet.of(ce1),
				noContacts());
		assertThat(merged, hasSize(1));
		assertThat(merged, hasItems(ce1));
	}

	@Test
	public void testLeftOnePersonWithOnePhoneNumbersAndRightOnePersonWithOnePhoneNumbers() {
		Contact ce1 = new ContactBuilder("Peter", "Fichtner").addPhoneNumber(
				"+49 175 123456").build();
		Contact ce2 = new ContactBuilder("Foo", "Bar").addPhoneNumber(
				"+49 171 66666").build();
		Set<Contact> merged = this.merger.merge(ImmutableSet.of(ce1),
				ImmutableSet.of(ce2));
		assertThat(merged, hasSize(2));
		assertThat(merged, hasItems(ce1, ce2));
	}

	@Test
	public void testLeftOnePersonWithOnePhoneNumberInTwoStyles() {
		Contact ce1 = new ContactBuilder("Peter", "Fichtner").addPhoneNumber(
				"+49 175 123456").build();
		Contact ce2 = new ContactBuilder("Name does not care ",
				"since it's taken from ce1").addPhoneNumber("0049 175 123456")
				.build();
		Set<Contact> merged = this.merger.merge(ImmutableSet.of(ce1),
				ImmutableSet.of(ce2));
		assertThat(merged, hasSize(1));
		assertThat(merged, hasItems(ce1));
	}

	@Test
	public void testLeftOnePersonWithOnePhoneNumbersAndRightOnePersonWithOnePhoneNumbers_butBothHaveTheSameNumber() {
		Contact ce1 = new ContactBuilder("Peter", "Fichtner").addPhoneNumber(
				"+49 175 123456").build();
		Contact ce2 = new ContactBuilder("Foo", "Bar").addPhoneNumber(
				"+49 175 123456").build();
		Set<Contact> merged = this.merger.merge(ImmutableSet.of(ce1),
				ImmutableSet.of(ce2));
		assertThat(merged, hasSize(1));
		assertThat(merged, hasItems(ce1));
	}

	@Test
	public void test10Left10RightNumber() {
		Builder<Contact> l = ImmutableSet.builder();
		Builder<Contact> exp = ImmutableSet.builder();
		for (int i = 0; i < 10; i++) {
			Contact ce = new ContactBuilder("Foo" + i, "Left" + i)
					.addPhoneNumber(
							"+49 171 " + Strings.repeat(String.valueOf(i), 6))
					.build();
			l.add(ce);
			exp.add(ce);
		}
		Builder<Contact> r = ImmutableSet.builder();
		for (int i = 0; i < 10; i++) {
			Contact ce = new ContactBuilder("Foo" + i, "Right" + i)
					.addPhoneNumber(
							"+49 178 " + Strings.repeat(String.valueOf(i), 6))
					.build();
			r.add(ce);
			exp.add(ce);
		}
		assertEquals(exp.build(), this.merger.merge(l.build(), r.build()));
	}

	@Test
	public void testBothSidesHaveSamePersonButWithDifferentNumbers() {
		Contact leftOnly = new ContactBuilder("Foo", "LeftOnly")
				.addPhoneNumber("+49 171 123456").build();
		Contact rightOnly = new ContactBuilder("Foo", "RightOnly")
				.addPhoneNumber("+49 179 173102").build();

		String nl = "+49 171 111111";
		String nr = "+49 172 222222";
		String givenName = "Bar";
		String familyName = "Both Sides";
		Contact left = new ContactBuilder(givenName, familyName)
				.addPhoneNumber(nl).build();
		Contact right = new ContactBuilder(givenName, familyName)
				.addPhoneNumber(nr).build();

		Set<Contact> lefts = ImmutableSet.of(leftOnly, left);
		Set<Contact> rights = ImmutableSet.of(right, rightOnly);

		Set<Contact> merged = this.merger.merge(lefts, rights);
		Contact combined = new ContactBuilder(givenName, familyName)
				.addPhoneNumber(nl).addPhoneNumber(nr).build();
		assertThat(merged, hasSize(3));
		assertThat(merged, hasItems(leftOnly, combined, rightOnly));
	}

	// --------------------------------------------------------------------------

	@Test
	public void testMergeDifferentPhoneNumberTypes() {
		String noLastname = null;
		Contact a = new ContactBuilder("W", "B")
				.addPhoneNumber(MOBILE, "01717654321")
				.addPhoneNumber(HOME, "01234 9988").build();
		Contact b = new ContactBuilder("Brother", noLastname)
				.addPhoneNumber(WORK, "012349988")
				.addPhoneNumber(CAR, "+49 171 7654321") // <- Starts with +49
														// and contains
														// whitespaces (and is
														// of type CAR)
				.addPhoneNumber(MOBILE, "012343998877665544").build();
		Set<Contact> merged = this.merger.merge(ImmutableSet.of(a),
				ImmutableSet.of(b));
		Contact exp1 = new ContactBuilder("W", "B")
				.addPhoneNumber(MOBILE, "01717654321")
				.addPhoneNumber(HOME, "01234 9988").build();
		Contact exp2 = new ContactBuilder("Brother", noLastname)
				.addPhoneNumber(MOBILE, "012343998877665544").build();
		assertThat(merged, hasSize(2));
		assertThat(merged, hasItems(exp1, exp2));
	}

}
