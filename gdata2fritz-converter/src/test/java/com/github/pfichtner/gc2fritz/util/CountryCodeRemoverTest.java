package com.github.pfichtner.gc2fritz.util;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.google.common.base.Function;

public class CountryCodeRemoverTest {

	@Test
	public void testEquals() {
		assertThat(CountryCodeRemover.forCC("+49"),
				is(CountryCodeRemover.forCC("0049")));
	}

	@Test
	public void testCountryCodeReplacing() {
		Function<String, String> normalizer = CountryCodeRemover.forCC("0049");
		assertThat(normalizer.apply("12345"), is("12345"));
		assertThat(normalizer.apply("0721 12345"), is("0721 12345"));
		assertThat(normalizer.apply("+49 721 12345"), is("0721 12345"));
		assertThat(normalizer.apply("0049 721 12345"), is("0721 12345"));

		assertThat(normalizer.apply("+01 987 12345"), is("0001 987 12345"));
		assertThat(normalizer.apply("0001 987 12345"), is("0001 987 12345"));
	}

}
