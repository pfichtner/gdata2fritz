package com.github.pfichtner.gc2fritz.util;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.google.common.collect.FluentIterable;

public class NormalizersTest {

	@Test
	public void testUnify() {
		List<String> in = Arrays.asList("+49 721999", "0721/888",
				"0049 721777", "+01 123666", "0001123555");
		List<String> expected = Arrays.asList("0049721999", "0049721888",
				"0049721777", "0001123666", "0001123555");
		assertThat(FluentIterable.from(in).transform(Normalizers.unifyNumber)
				.toList(), is(expected));
	}

}
