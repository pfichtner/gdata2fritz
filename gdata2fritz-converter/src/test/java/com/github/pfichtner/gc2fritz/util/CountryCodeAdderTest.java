package com.github.pfichtner.gc2fritz.util;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.google.common.base.Function;

public class CountryCodeAdderTest {

	@Test
	public void testEquals() {
		assertThat(CountryCodeAdder.forCC("+49"),
				is(CountryCodeAdder.forCC("0049")));
	}

	@Test
	public void testAdd00() {
		check(CountryCodeAdder.forCC("0049"));
	}

	@Test
	public void testAddPlus() {
		check(CountryCodeAdder.forCC("+49"));
	}

	private void check(Function<String, String> adder) {
		assertThat(adder.apply("0049721..."), is("0049721..."));
		assertThat(adder.apply("0721..."), is("0049721..."));
		assertThat(adder.apply("0049721..."), is("0049721..."));
		assertThat(adder.apply("0001123..."), is("0001123..."));
		assertThat(adder.apply("0001123..."), is("0001123..."));
	}

}
