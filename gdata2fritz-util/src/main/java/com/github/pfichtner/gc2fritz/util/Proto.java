package com.github.pfichtner.gc2fritz.util;

import static com.google.common.base.Preconditions.checkState;

import java.net.URI;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Proto {

	HTTP("http", 80), HTTPS("https", 443);

	@Getter
	private final String scheme;
	@Getter
	private final int defaultPort;

	@Nonnull
	public static Proto forScheme(String scheme) {
		Proto proto = forScheme_(scheme);
		checkState(proto != null, "%s not a valid scheme", scheme);
		return proto;
	}

	@Nonnull
	public static Proto ofUri(URI uri) {
		return forScheme(uri.getScheme());
	}

	@Nonnull
	public static Proto ofUri(URI uri, Proto defValue) {
		return forScheme(uri.getScheme(), defValue);
	}

	@Nonnull
	public static Proto forScheme(String scheme, Proto defValue) {
		Proto proto = forScheme_(scheme);
		return proto == null ? defValue : proto;
	}

	@Nullable
	private static Proto forScheme_(String scheme) {
		for (Proto proto : values()) {
			if (proto.getScheme().equalsIgnoreCase(scheme)) {
				return proto;
			}
		}
		return null;
	}

}
