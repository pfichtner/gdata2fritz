package com.github.pfichtner.gc2fritz.util;

import java.net.URI;
import java.net.URISyntaxException;

import lombok.extern.slf4j.Slf4j;

import com.google.common.base.Throwables;

@Slf4j
public class URIs {

	public static URI newURI(String string) {
		return newURI(string, Proto.HTTP);
	}

	public static URI newURI(String string, Proto defaultProto) {
		return newURI(string, defaultProto.getScheme() + "://");
	}

	public static URI newURI(String string, String defaultScheme) {
		try {
			if (string.contains("://")) {
				return new URI(string);
			}
			log.warn("Proto missing on {}, using {}", string, defaultScheme);
			return new URI(defaultScheme + string);
		} catch (URISyntaxException e) {
			throw Throwables.propagate(e);
		}
	}

	public static int getPort(URI uri) {
		Proto proto = Proto.ofUri(uri);
		return uri.getPort() >= 0 ? uri.getPort() : proto.getDefaultPort();
	}

}
