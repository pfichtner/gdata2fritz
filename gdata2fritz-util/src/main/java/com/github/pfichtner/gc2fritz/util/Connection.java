package com.github.pfichtner.gc2fritz.util;

import static com.google.common.base.Preconditions.checkNotNull;

import java.net.URI;

import javax.annotation.Nonnull;

import lombok.Data;

/**
 * A connection class that guarantees to have proto (scheme), host and port set.
 * If these values are absent in the passed String the fallback proto (and port)
 * will be used.
 * 
 * @author Peter Fichtner
 */
@Data
public class Connection {

	private final Proto proto;
	private final String host;
	private final int port;

	public Connection(@Nonnull String uri, @Nonnull Proto defaultProto) {
		this(URIs.newURI(uri), defaultProto);
	}

	public Connection(URI uri, Proto defaultProto) {
		this.proto = Proto.ofUri(checkNotNull(uri, "uri"),
				checkNotNull(defaultProto, "defaultProto"));
		this.host = checkNotNull(uri.getHost(), "host");
		this.port = URIs.getPort(uri);
	}

}
