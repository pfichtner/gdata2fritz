package com.github.pfichtner.gc2fritz.util;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;

import lombok.RequiredArgsConstructor;

import com.google.common.base.Optional;
import com.google.common.collect.ForwardingMap;
import com.google.common.collect.Sets;

/**
 * A map that keeps track of keys that have been accessed (no matter if there
 * are values for that key). Additionally there is an {@link Optional} wrapping
 * method named {@link #getOptional(Object)}.
 * 
 * @author Peter Fichtner
 * 
 * @param <K>
 * @param <V>
 */
@RequiredArgsConstructor(staticName = "of")
public class TrackingMap<K, V> extends ForwardingMap<K, V> {

	private final Map<K, V> delegate;
	private final Set<Object> retrievedKeys = Sets.newHashSet();
	private final Collection<Object> retrievedKeysView = Collections
			.unmodifiableCollection(retrievedKeys);

	@Override
	protected Map<K, V> delegate() {
		return this.delegate;
	}

	@Override
	public V get(Object key) {
		this.retrievedKeys.add(key);
		return super.get(key);
	}

	@Nonnull
	public V getChecked(K key) {
		return checkNotNull(get(key), "%s is not set", key);
	}

	@Nonnull
	public Optional<V> getOptional(K key) {
		this.retrievedKeys.add(key);
		return Optional.fromNullable(super.get(key));
	}

	public Collection<Object> getRetrievedKeys() {
		return this.retrievedKeysView;
	}

	public Collection<K> getUnretrievedKeys() {
		Set<K> unretrieved = Sets.newHashSet(keySet());
		unretrieved.removeAll(getRetrievedKeys());
		return unretrieved;
	}

}
