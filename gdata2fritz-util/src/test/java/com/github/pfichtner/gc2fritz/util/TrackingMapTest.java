package com.github.pfichtner.gc2fritz.util;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;

public class TrackingMapTest {

	@Test
	public void testGet() {
		TrackingMap<String, Integer> tm = TrackingMap.of(ImmutableMap.of("a",
				1, "b", 2, "d", 4));
		assertThat(tm.get("a"), is(notNullValue()));
		assertThat(tm.get("b"), is(notNullValue()));
		assertThat(tm.get("c"), is(nullValue()));
		assertThat(tm.get("d"), is(notNullValue()));
		assertThat(Sets.newHashSet(tm.getRetrievedKeys()),
				is(Sets.<Object> newHashSet("a", "b", "c", "d")));
	}

	@Test
	public void testGetOptional() {
		TrackingMap<String, Integer> tm = TrackingMap.of(ImmutableMap.of("a",
				1, "b", 2, "d", 4));
		assertTrue(tm.getOptional("a").isPresent());
		assertTrue(tm.getOptional("b").isPresent());
		assertFalse(tm.getOptional("c").isPresent());
		assertTrue(tm.getOptional("d").isPresent());
		assertThat(Sets.newHashSet(tm.getRetrievedKeys()),
				is(Sets.<Object> newHashSet("a", "b", "c", "d")));
	}

	@Test
	public void testGetUnretrievedKeys() {
		TrackingMap<String, Integer> tm = TrackingMap.of(ImmutableMap.of("a",
				1, "b", 2, "d", 4));
		tm.get("a");
		tm.get("d");
		assertEquals(Sets.newHashSet("b"), tm.getUnretrievedKeys());
	}

}
