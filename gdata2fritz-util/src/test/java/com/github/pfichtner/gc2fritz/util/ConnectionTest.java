package com.github.pfichtner.gc2fritz.util;

import static org.junit.Assert.assertEquals;

import java.net.URISyntaxException;

import org.junit.Test;

public class ConnectionTest {

	@Test
	public void testPlainHost() throws URISyntaxException {
		Connection connection = new Connection("fritz.box", Proto.HTTP);
		assertEquals(Proto.HTTP, connection.getProto());
		assertEquals("fritz.box", connection.getHost());
		assertEquals(80, connection.getPort());
	}

	@Test
	public void testHostWithProto() throws URISyntaxException {
		Connection connection = new Connection("https://fritz.box", Proto.HTTP);
		assertEquals(Proto.HTTPS, connection.getProto());
		assertEquals("fritz.box", connection.getHost());
		assertEquals(443, connection.getPort());
	}

	@Test
	public void testHostWithPort() throws URISyntaxException {
		Connection connection = new Connection("fritz.box:444", Proto.HTTP);
		assertEquals(Proto.HTTP, connection.getProto());
		assertEquals("fritz.box", connection.getHost());
		assertEquals(444, connection.getPort());
	}

	@Test
	public void testHostWithProtoAndPort() throws URISyntaxException {
		Connection connection = new Connection("https://fritz.box:444",
				Proto.HTTP);
		assertEquals(Proto.HTTPS, connection.getProto());
		assertEquals("fritz.box", connection.getHost());
		assertEquals(444, connection.getPort());
	}

}
