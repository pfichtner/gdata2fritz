package com.github.pfichtner.gc2fritz.util;

import static org.junit.Assert.assertThat;

import java.net.URI;
import java.net.URISyntaxException;

import org.hamcrest.core.Is;
import org.junit.Test;

public class URIsTest {

	@Test
	public void testX() throws URISyntaxException {
		assertThat(URIs.newURI("fritz.box"), Is.is(new URI("http://fritz.box")));
		assertThat(URIs.newURI("http://fritz.box"), Is.is(new URI("http://fritz.box")));
	}

}
