package com.github.pfichtner.gc2fritz.fbcom.main;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class LongOpts {

	public static final String LONGOPT_HOST = "host";
	public static final String LONGOPT_USERNAME = "username";
	public static final String LONGOPT_PASSWORD = "password";

	public static final String LONGOPT_PHONEBOOKFILE = "phoneBookFile";
	public static final String LONGOPT_PHONEBOOK_ID = "phoneBookId";

}
