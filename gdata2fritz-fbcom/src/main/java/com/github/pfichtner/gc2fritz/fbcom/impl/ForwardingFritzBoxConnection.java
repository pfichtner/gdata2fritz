package com.github.pfichtner.gc2fritz.fbcom.impl;

import lombok.AccessLevel;
import lombok.Delegate;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import com.github.pfichtner.gc2fritz.fbcom.api.FritzBoxConnection;

@RequiredArgsConstructor
public class ForwardingFritzBoxConnection implements FritzBoxConnection {

	@Delegate
	@Setter(value = AccessLevel.PROTECTED)
	@Getter(value = AccessLevel.PROTECTED)
	private FritzBoxConnection delegate;

}
