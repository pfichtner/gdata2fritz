package com.github.pfichtner.gc2fritz.fbcom.main;

import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_HOST;
import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_USERNAME;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_HOST;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_PASSWORD;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_USERNAME;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import com.github.pfichtner.gc2fritz.fbcom.api.FritzBoxConnection;
import com.github.pfichtner.gc2fritz.fbcom.impl.AutoDetectFritzBoxConnection;
import com.github.pfichtner.gc2fritz.util.Connection;
import com.github.pfichtner.gc2fritz.util.Proto;

/**
 * Activates or deactivates silent mode (Klingelsperre)
 * 
 * @author Peter Fichtner
 */
@Slf4j
public class Silent {

	@Option(name = "-h", aliases = "--" + LONGOPT_HOST, usage = "URL of the fritzbox")
	private String host = DEFAULT_HOST;

	@Option(name = "-u", aliases = "--" + LONGOPT_USERNAME, usage = "username of the fritzbox")
	private String username = DEFAULT_USERNAME;

	@Option(name = "-p", aliases = "--" + LONGOPT_PASSWORD, required = true, usage = "password of the fritzbox")
	private String password;

	@Option(name = "-s", aliases = "--state", required = true, usage = "silent or unsilent")
	private String state;

	public static void main(String[] args) throws Exception {
		new Silent().run(args);
	}

	private void run(String[] args) throws Exception {
		CmdLineParser cmdLineParser = new CmdLineParser(this);
		try {
			cmdLineParser.parseArgument(args);
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			cmdLineParser.printUsage(System.err);
			return;
		}
		@Cleanup
		FritzBoxConnection fbConnect = new AutoDetectFritzBoxConnection(
				new Connection(this.host, Proto.HTTP));
		fbConnect.login(this.username, this.password);

		try {
			fbConnect.allTelephonesSilent(Boolean.parseBoolean(this.state));
		} catch (Exception e) {
			log.error("Error setting silent mode", e);
		}

	}

}
