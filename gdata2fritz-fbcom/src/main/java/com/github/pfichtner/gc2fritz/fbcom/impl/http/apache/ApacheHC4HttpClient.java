package com.github.pfichtner.gc2fritz.fbcom.impl.http.apache;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.http.ConnectionReuseStrategy;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.DefaultHttpClientConnection;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.message.BasicHttpRequest;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HttpRequestExecutor;
import org.apache.http.protocol.ImmutableHttpProcessor;
import org.apache.http.protocol.RequestConnControl;
import org.apache.http.protocol.RequestContent;
import org.apache.http.protocol.RequestExpectContinue;
import org.apache.http.protocol.RequestTargetHost;
import org.apache.http.protocol.RequestUserAgent;
import org.apache.http.util.EntityUtils;

import com.github.pfichtner.gc2fritz.fbcom.impl.http.HttpClient;
import com.github.pfichtner.gc2fritz.util.Proto;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;

public class ApacheHC4HttpClient implements HttpClient {

	public class ApacheBuilder implements Builder {

		private final String uri;
		private final Map<String, Object> data = Maps.newLinkedHashMap();
		private Mode mode = Mode.URLENCODED;

		public ApacheBuilder(String uri) {
			this.uri = uri;
		}

		@Override
		public Builder add(String key, String value) {
			this.data.put(key, value);
			return this;
		}

		@Override
		public Builder add(String key, byte[] value) {
			this.data.put(key, value);
			return this;
		}

		@Override
		public Builder mode(Mode mode) {
			this.mode = mode;
			return this;
		}

		@Override
		public String doGet() throws IOException, URISyntaxException {
			return doGet(this.uri, this.data);
		}

		private String doGet(String uri, Map<String, Object> data)
				throws URISyntaxException, IOException {
			try {
				String uriWithParams = data.isEmpty() ? uri : uri + '?'
						+ Joiner.on('&').withKeyValueSeparator("=").join(data);
				return exec(new BasicHttpRequest("GET", uriWithParams));
			} catch (HttpException e) {
				throw new IOException(e);
			}
		}

		@Override
		public String doPost() throws IOException, URISyntaxException {
			BasicHttpEntityEnclosingRequest request = new BasicHttpEntityEnclosingRequest(
					"POST", this.uri);
			request.setEntity(createEntity(request));
			try {
				return exec(request);
			} catch (HttpException e) {
				throw new IOException(e);
			}
		}

		private HttpEntity createEntity(BasicHttpEntityEnclosingRequest request)
				throws UnsupportedEncodingException {
			if (this.mode == Mode.URLENCODED) {
				request.addHeader("content-type",
						"application/x-www-form-urlencoded");
				return createUrlEncoded();
			} else if (this.mode == Mode.FORMDATA) {
				return createFormdata();
			} else {
				throw new IllegalStateException("Cannot handle " + this.mode);
			}
		}

		private HttpEntity createUrlEncoded()
				throws UnsupportedEncodingException {
			return new StringEntity(Joiner.on('&').withKeyValueSeparator("=")
					.join(this.data), Charsets.UTF_8.displayName());
		}

		private HttpEntity createFormdata() throws UnsupportedEncodingException {
			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			for (Entry<String, Object> entry : this.data.entrySet()) {
				entity.addPart(entry.getKey(),
						createContent(entry.getKey(), entry.getValue()));
			}
			return entity;
		}

		private ContentBody createContent(String key, Object value)
				throws UnsupportedEncodingException {
			if (value instanceof String) {
				return new StringBody((String) value);
			} else if (value instanceof byte[]) {
				return new ByteArrayBody((byte[]) value, key);
			} else {
				throw new IllegalStateException("Cannot handle "
						+ value.getClass());
			}
		}

	}

	private final SyncBasicHttpParams params = createSyncBasicHttpParams();
	private final ImmutableHttpProcessor httpproc = new ImmutableHttpProcessor(
			new HttpRequestInterceptor[] { new RequestContent(),
					new RequestTargetHost(), new RequestConnControl(),
					new RequestUserAgent(), new RequestExpectContinue() });
	private final HttpRequestExecutor httpexecutor = new HttpRequestExecutor();
	private final BasicHttpContext context;
	private final DefaultHttpClientConnection connection = new DefaultHttpClientConnection();
	private final ConnectionReuseStrategy connStrategy = new DefaultConnectionReuseStrategy();

	private final HttpHost host;

	public ApacheHC4HttpClient(String hostname, int port) {
		this(Proto.HTTP, hostname, port);
	}

	public ApacheHC4HttpClient(Proto proto, String hostname, int port) {
		this.host = new HttpHost(hostname, port, proto.getScheme());
		this.context = new BasicHttpContext(null);
		this.context.setAttribute(ExecutionContext.HTTP_CONNECTION,
				this.connection);
		this.context.setAttribute(ExecutionContext.HTTP_TARGET_HOST, this.host);
	}

	private static SyncBasicHttpParams createSyncBasicHttpParams() {
		SyncBasicHttpParams params = new SyncBasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params,
				Charsets.UTF_8.displayName());
		HttpProtocolParams.setUserAgent(params, "HttpComponents/1.1");
		HttpProtocolParams.setUseExpectContinue(params, true);
		HttpConnectionParams.setConnectionTimeout(params,
				Ints.checkedCast(TimeUnit.MINUTES.toMillis(1)));
		HttpConnectionParams.setSoTimeout(params,
				Ints.checkedCast(TimeUnit.SECONDS.toMillis(10)));
		return params;
	}

	@Override
	public Builder builder(String uri) {
		return new ApacheBuilder(uri);
	}

	private void preRequest() throws UnknownHostException, IOException {
		if (!this.connection.isOpen()) {
			this.connection.bind(
					new Socket(this.host.getHostName(), this.host.getPort()),
					this.params);
		}
	}

	private String exec(BasicHttpRequest request) throws UnknownHostException,
			IOException, HttpException {
		preRequest();

		request.setParams(this.params);
		this.httpexecutor.preProcess(request, this.httpproc, this.context);
		HttpResponse response = this.httpexecutor.execute(request,
				this.connection, this.context);
		response.setParams(this.params);
		this.httpexecutor.postProcess(response, this.httpproc, this.context);
		String result = EntityUtils.toString(response.getEntity());

		if (!this.connStrategy.keepAlive(response, this.context)) {
			this.connection.close();
		}

		return result;
	}

	public void close() throws IOException {
		this.connection.close();
	}

}
