package com.github.pfichtner.gc2fritz.fbcom.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Collection;

import lombok.extern.slf4j.Slf4j;

import com.github.pfichtner.gc2fritz.util.Connection;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

@Slf4j
public class SessionIdFritzBoxConnection extends AbstractFritzBoxConnection {

	public SessionIdFritzBoxConnection(Connection connection)
			throws UnknownHostException, IOException, URISyntaxException {
		super(connection);
	}

	@Override
	public boolean tryLogin(String username, String password)
			throws IOException, URISyntaxException {
		String httpResponse = getChallenge();
		SessionInfo si = toSessionInfo(httpResponse);
		if (!si.isWriteAccess()) {
			String challenge = si.getChallenge();
			log.debug("Got challenge {}", challenge);
			String response = getResponse(challenge, password);
			log.debug("Calculated response {}", response);
			httpResponse = getHttpClient().builder("/cgi-bin/webcm")
					.add("login:command/response", response)
					.add("getpage", "../html/login_sid.xml").doPost();
		}
		setSid(toSessionInfo(httpResponse).getSid());
		return isSidOk();
	}

	public Collection<String> listCalls() throws UnknownHostException,
			IOException, URISyntaxException {
		String getResponse = getHttpClient().builder("/cgi-bin/webcm")
				.add("getpage", "../html/de/FRITZ!Box_Anrufliste.csv")
				.add("var:pagename", "home")
				.add("errorpage", "../html/de/FRITZ!Box_Anrufliste.csv")
				.add("sid", getSid()).doPost();

		// TODO filter empty lines
		return Lists.newArrayList(Splitter.on(CharMatcher.anyOf("\r\n")).split(
				getResponse));
	}
}
