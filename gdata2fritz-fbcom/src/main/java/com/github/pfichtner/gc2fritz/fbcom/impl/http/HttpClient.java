package com.github.pfichtner.gc2fritz.fbcom.impl.http;

import java.io.IOException;
import java.net.URISyntaxException;

public interface HttpClient {

	public static enum Mode {
		URLENCODED, FORMDATA;
	}

	public interface Builder {

		Builder add(String key, String value);

		Builder add(String key, byte[] value);

		Builder mode(Mode mode);

		String doGet() throws IOException, URISyntaxException;

		String doPost() throws IOException, URISyntaxException;

	}

	Builder builder(String uri);

	void close() throws IOException;

}