package com.github.pfichtner.gc2fritz.fbcom.impl;

import lombok.RequiredArgsConstructor;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;

@RequiredArgsConstructor
public class CRCreator {

	private final String password;

	public String challenge(String challenge) {
		return challenge + '-' + createHash(challenge);
	}

	private String createHash(String challenge) {
		String string = challenge + '-' + this.password;
		return Hashing.md5().hashString(string, Charsets.UTF_16LE).toString()
				.toLowerCase();
	}

}
