package com.github.pfichtner.gc2fritz.fbcom.main;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class Defaults {

	public static final String DEFAULT_HOST = "http://fritz.box";
	public static final String DEFAULT_USERNAME = "";
	public static final int DEFAULT_PHONEBOOK_ID = 0;

}
