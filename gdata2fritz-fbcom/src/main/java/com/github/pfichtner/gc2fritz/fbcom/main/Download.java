package com.github.pfichtner.gc2fritz.fbcom.main;

import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_HOST;
import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_PHONEBOOK_ID;
import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_USERNAME;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_HOST;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_PASSWORD;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_PHONEBOOKFILE;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_PHONEBOOK_ID;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_USERNAME;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import com.github.pfichtner.gc2fritz.fbcom.api.FritzBoxConnection;
import com.github.pfichtner.gc2fritz.fbcom.impl.AutoDetectFritzBoxConnection;
import com.github.pfichtner.gc2fritz.fbcom.util.PhonebookIds;
import com.github.pfichtner.gc2fritz.util.Connection;
import com.github.pfichtner.gc2fritz.util.Proto;

/**
 * Retrieves the phonebook from a FritzBox.
 * 
 * @author Peter Fichtner
 */
@Slf4j
public class Download {

	@Option(name = "-h", aliases = "--" + LONGOPT_HOST, usage = "URL of the fritzbox")
	private String host = DEFAULT_HOST;

	@Option(name = "-u", aliases = "--" + LONGOPT_USERNAME, usage = "username of the fritzbox")
	private String username = DEFAULT_USERNAME;

	@Option(name = "-p", aliases = "--" + LONGOPT_PASSWORD, required = true, usage = "password of the fritzbox")
	private String password;

	@Option(name = "-f", aliases = "--" + LONGOPT_PHONEBOOKFILE, usage = "file (phonebook.xml) to download to, file get's overwritten!")
	private File phoneBookFile;

	@Option(name = "-i", aliases = "--" + LONGOPT_PHONEBOOK_ID, usage = "phonebook id on the fritzbox")
	private String phoneBookId = String.valueOf(DEFAULT_PHONEBOOK_ID);

	public static void main(String[] args) throws Exception {
		new Download().run(args);
	}

	private void run(String[] args) throws Exception {
		CmdLineParser cmdLineParser = new CmdLineParser(this);
		try {
			cmdLineParser.parseArgument(args);
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			cmdLineParser.printUsage(System.err);
			return;
		}
		@Cleanup
		FritzBoxConnection fbConnect = new AutoDetectFritzBoxConnection(
				new Connection(this.host, Proto.HTTP));
		fbConnect.login(this.username, this.password);

		if (this.phoneBookFile == null) {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			try {
				fbConnect.downloadPhoneBook(os,
						PhonebookIds.resolvePhonebookIdIfNeeded(fbConnect,
								this.phoneBookId));
				System.out.println(new String(os.toByteArray()));
			} catch (Exception e) {
				log.error("Error downloading phonebook {}", this.phoneBookId, e);
			} finally {
				os.close();
			}
		} else {
			log.info("Downloading to {}", this.phoneBookFile);
			@Cleanup
			FileOutputStream os = new FileOutputStream(this.phoneBookFile);
			try {
				fbConnect.downloadPhoneBook(os,
						PhonebookIds.resolvePhonebookIdIfNeeded(fbConnect,
								this.phoneBookId));
				log.info("{} downloaded successfully", this.phoneBookFile);
			} catch (Exception e) {
				log.error("Error downloading {}", this.phoneBookFile);
			}
		}

	}

}
