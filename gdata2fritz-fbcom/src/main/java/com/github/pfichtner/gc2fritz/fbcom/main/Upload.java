package com.github.pfichtner.gc2fritz.fbcom.main;

import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_HOST;
import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_PHONEBOOK_ID;
import static com.github.pfichtner.gc2fritz.fbcom.main.Defaults.DEFAULT_USERNAME;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_HOST;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_PASSWORD;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_PHONEBOOKFILE;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_PHONEBOOK_ID;
import static com.github.pfichtner.gc2fritz.fbcom.main.LongOpts.LONGOPT_USERNAME;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import com.github.pfichtner.gc2fritz.fbcom.api.FritzBoxConnection;
import com.github.pfichtner.gc2fritz.fbcom.impl.AutoDetectFritzBoxConnection;
import com.github.pfichtner.gc2fritz.fbcom.util.PhonebookIds;
import com.github.pfichtner.gc2fritz.util.Connection;
import com.github.pfichtner.gc2fritz.util.Proto;

@Slf4j
public class Upload {

	@Option(name = "-h", aliases = "--" + LONGOPT_HOST, usage = "URL of the fritzbox")
	private String host = DEFAULT_HOST;

	@Option(name = "-u", aliases = "--" + LONGOPT_USERNAME, usage = "username of the fritzbox")
	private String username = DEFAULT_USERNAME;

	@Option(name = "-p", aliases = "--" + LONGOPT_PASSWORD, required = true, usage = "password of the fritzbox")
	private String password;

	@Option(name = "-f", aliases = "--" + LONGOPT_PHONEBOOKFILE, required = true, usage = "file (phonebook.xml) to upload")
	private File phoneBookFile;

	@Option(name = "-i", aliases = "--" + LONGOPT_PHONEBOOK_ID, usage = "phonebook id on the fritzbox")
	private String phoneBookId = String.valueOf(DEFAULT_PHONEBOOK_ID);

	public static void main(String[] args) throws Exception {
		new Upload().run(args);
	}

	private void run(String[] args) throws Exception {
		CmdLineParser cmdLineParser = new CmdLineParser(this);
		try {
			cmdLineParser.parseArgument(args);
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			cmdLineParser.printUsage(System.err);
			return;
		}
		@Cleanup
		FritzBoxConnection fbConnect = new AutoDetectFritzBoxConnection(
				new Connection(this.host, Proto.HTTP));
		fbConnect.login(this.username, this.password);

		log.info("Uploading {}", this.phoneBookFile);
		@Cleanup
		InputStream is = new FileInputStream(this.phoneBookFile);
		try {
			fbConnect.uploadPhoneBook(is, PhonebookIds
					.resolvePhonebookIdIfNeeded(fbConnect, this.phoneBookId));
			log.info("{} uploaded successfully", this.phoneBookFile);
		} catch (Exception e) {
			log.error("Error uploading {}", this.phoneBookFile, e);
		}
	}

}
