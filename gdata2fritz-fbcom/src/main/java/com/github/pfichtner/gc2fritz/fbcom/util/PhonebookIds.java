package com.github.pfichtner.gc2fritz.fbcom.util;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import com.github.pfichtner.gc2fritz.fbcom.api.FritzBoxConnection;
import com.google.common.collect.HashBiMap;
import com.google.common.primitives.Ints;

@Slf4j
public class PhonebookIds {

	public static int resolvePhonebookIdIfNeeded(FritzBoxConnection fbConnect,
			String phoneBookValue) throws Exception {
		Integer parsed = Ints.tryParse(phoneBookValue);
		if (parsed != null) {
			return parsed.intValue();
		}
		log.debug("{} not an integer value, resolving id", phoneBookValue);
		int resolved = resolvePhonebook(fbConnect, phoneBookValue);
		log.info("name {} resolved to id {}", phoneBookValue,
				Integer.valueOf(resolved));
		return resolved;
	}

	private static int resolvePhonebook(FritzBoxConnection fbConnect,
			String phoneBookValue) throws Exception {
		Map<String, Integer> reversed = HashBiMap.create(
				fbConnect.listPhoneBooks()).inverse();
		Integer integer = reversed.get(phoneBookValue);
		return checkNotNull(integer,
				"Unable to resolve phoneBook %s, available names are",
				phoneBookValue, reversed.keySet()).intValue();
	}

}
