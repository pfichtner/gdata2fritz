package com.github.pfichtner.gc2fritz.fbcom.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Cleanup;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import com.github.pfichtner.gc2fritz.fbcom.api.FritzBoxConnection;
import com.github.pfichtner.gc2fritz.fbcom.impl.http.HttpClient;
import com.github.pfichtner.gc2fritz.fbcom.impl.http.HttpClient.Builder;
import com.github.pfichtner.gc2fritz.fbcom.impl.http.HttpClient.Mode;
import com.github.pfichtner.gc2fritz.fbcom.impl.http.google.GoogleHttpClient;
import com.github.pfichtner.gc2fritz.util.Connection;
import com.google.common.base.CharMatcher;
import com.google.common.collect.Maps;
import com.google.common.io.ByteStreams;

@Slf4j
@RequiredArgsConstructor
public abstract class AbstractFritzBoxConnection implements FritzBoxConnection {

	@XmlRootElement(name = "SessionInfo")
	@Getter
	@EqualsAndHashCode
	@ToString
	static class SessionInfo {

		private boolean writeAccess;

		@XmlElement(name = "SID")
		private String sid;

		@XmlElement(name = "Challenge")
		private String challenge;

		@XmlElement(name = "iswriteaccess")
		public void setIswriteaccess(String iswriteaccess) {
			this.writeAccess = "1".equals(iswriteaccess);
		}
	}

	private static final String UPLOAD_SUCCESS = checkNotNull(ResourceBundle
			.getBundle("FritBoxConnect").getString("upload.success"),
			"Could not determine \"upload.success\"");

	@Getter
	private final HttpClient httpClient;

	@Getter
	private String sid;

	public AbstractFritzBoxConnection(Connection connection) {
		this(new GoogleHttpClient(connection.getProto(), connection.getHost(),
				connection.getPort()));
		// this(new ApacheHC4HttpClient(proto, hostname, port));
	}

	@Override
	public AbstractFritzBoxConnection login(String username, String password)
			throws Exception {
		checkState(tryLogin(username, password), "Invalid SID %s", getSid());
		return this;
	}

	protected void setSid(String sid) {
		log.info("Login success, SID is {}", sid);
		this.sid = sid;
	}

	protected boolean isSidOk() {
		return !CharMatcher.is('0').matchesAllOf(
				checkNotNull(this.sid, "sid must not be null"));
	}

	protected SessionInfo toSessionInfo(String text) {
		try {
			JAXBContext context = JAXBContext.newInstance(SessionInfo.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			@Cleanup
			StringReader reader = new StringReader(text);
			return (SessionInfo) unmarshaller.unmarshal(reader);
		} catch (Exception e) {
			throw new RuntimeException("Could not parse " + text
					+ " as SessionInfo", e);
		}
	}

	protected String getChallenge() throws UnknownHostException, IOException,
			URISyntaxException {
		return this.httpClient.builder("/login_sid.lua").doGet();
	}

	protected String getResponse(String challenge, String password) {
		return new CRCreator(password).challenge(challenge);
	}

	// -------------------------------------------------------------------

	public void uploadPhoneBook(InputStream inputStream, int phoneBookId)
			throws UnknownHostException, IOException, URISyntaxException {
		String response = this.httpClient
				.builder("/cgi-bin/firmwarecfg")
				.add("sid", this.sid)
				.add("PhonebookId", String.valueOf(phoneBookId))
				.add("PhonebookImportFile",
						ByteStreams.toByteArray(inputStream))
				.mode(Mode.FORMDATA).doPost();
		checkState(response.contains(UPLOAD_SUCCESS),
				"Upload not sucessful: Could not find %s in %s",
				UPLOAD_SUCCESS, response);
	}

	// -------------------------------------------------------------------

	@Override
	public void downloadPhoneBook(OutputStream outputStream, int phoneBookId)
			throws Exception {
		outputStream.write(this.httpClient.builder("/cgi-bin/firmwarecfg")
				.add("sid", this.sid)
				.add("PhonebookId", String.valueOf(phoneBookId))
				.add("PhonebookExportName", "Telefonbuch")
				.add("PhonebookExport", "").mode(Mode.FORMDATA).doPost()
				.getBytes());
		// TODO check for valid (phonebook) XML?
	}

	public Map<Integer, String> listPhoneBooks() throws Exception {
		String response = this.httpClient
				.builder("/fon_num/fonbook_select.lua").add("sid", this.sid)
				.doGet();
		Matcher matcher = Pattern.compile(
				"<label\\sfor=\"uiBookid:([^\"]+)\">([^<]+)</label>").matcher(
				response);
		Map<Integer, String> result = Maps.newHashMap();
		while (matcher.find()) {
			result.put(Integer.valueOf(matcher.group(1)),
					CharMatcher.JAVA_ISO_CONTROL.removeFrom(matcher.group(2)));
		}
		return result;
	}

	// -------------------------------------------------------------------

	public void allTelephonesSilent(boolean onOff) throws Exception {
		Builder b = this.httpClient.builder("/system/ring_block.lua").add(
				"sid", this.sid);
		if (onOff) {
			b = b.add("active", "on").add("start_hour", "0")
					.add("start_minute", "00").add("end_hour", "23")
					.add("end_minute", "59");
		}
		String response = b.add("apply", "").mode(Mode.URLENCODED).doPost();

		StringBuilder sb = new StringBuilder()
				.append("<input type=\"checkbox\" name=\"active\" id=\"uiActive\"");
		String mustMatch = (onOff ? sb.append("checked") : sb).append(">")
				.toString();
		String replaceFrom = CharMatcher.anyOf("\r\n").removeFrom(response);
		checkState(replaceFrom.contains(mustMatch),
				"Switching silentMode not successful: Could not find %s in %s",
				mustMatch, replaceFrom);
	}

	// -------------------------------------------------------------------

	public void close() {
		log.debug("Closing connection");
		log.info("Logging out");
		try {
			this.httpClient.builder("/login_sid.lua").add("logout", "dummy")
					.add("sid", this.sid).doPost();
		} catch (Exception e) {
			log.warn("Error logging out", e);
		}
		try {
			this.httpClient.close();
		} catch (IOException e) {
			log.warn("Error closing connection", e);
		}
	}
}
