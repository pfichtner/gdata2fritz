package com.github.pfichtner.gc2fritz.fbcom.impl.http.google;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.github.pfichtner.gc2fritz.fbcom.impl.http.HttpClient;
import com.github.pfichtner.gc2fritz.util.Proto;
import com.google.api.client.http.AbstractHttpContent;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpMediaType;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.common.base.Throwables;
import com.google.common.collect.Maps;

@RequiredArgsConstructor
public class GoogleHttpClient implements HttpClient {

	@Accessors(fluent = true)
	@Setter
	@RequiredArgsConstructor
	public class GoogleBuilder implements Builder {

		private final String uri;
		private final Map<String, Object> data = Maps.newLinkedHashMap();
		private Mode mode = Mode.URLENCODED;

		public GoogleBuilder add(String key, String value) {
			this.data.put(key, value);
			return this;
		}

		public GoogleBuilder add(String key, byte[] value) {
			this.data.put(key, value);
			return this;
		}

		public GoogleBuilder mode(Mode mode) {
			this.mode = mode;
			return this;
		}

		@Override
		public String doGet() throws IOException, URISyntaxException {
			GenericUrl url = createGUrl(this.uri);
			url.putAll(this.data);
			HttpRequest request = requestFactory.buildGetRequest(url);
			HttpResponse response = request.execute();
			return response.parseAsString();
		}

		public String doPost() throws IOException, URISyntaxException {
			return doPost(this.uri, getContent());
		}

		private String doPost(String uri, AbstractHttpContent content)
				throws IOException, URISyntaxException {
			GenericUrl url = createGUrl(uri);
			HttpRequest request = requestFactory.buildPostRequest(url, content);
			HttpResponse response = request.execute();
			return response.parseAsString();
		}

		private AbstractHttpContent getContent() {
			return this.mode == Mode.URLENCODED && dataContainsStringsOnly() ? new UrlEncodedContent(
					this.data) : createMultipartFormDataContent(this.data);
		}

		private boolean dataContainsStringsOnly() {
			Collection<Object> values = this.data.values();
			for (Object object : values) {
				if (!(object instanceof String)) {
					return false;
				}
			}
			return true;
		}

		private AbstractHttpContent createMultipartFormDataContent(
				Map<String, Object> data) {
			MultipartFormDataContent content = new MultipartFormDataContent();
			content.setMediaType(new HttpMediaType("multipart/form-data")
					.setParameter("boundary", UUID.randomUUID().toString()));
			for (Entry<String, Object> entry : data.entrySet()) {
				content.addPart(new MultipartFormDataContent.Part().setName(
						entry.getKey()).setContent(
						createContent(entry.getValue())));
			}
			return content;
		}

		private HttpContent createContent(Object value) {
			checkNotNull(value);
			if (value instanceof String) {
				return new ByteArrayContent(null, ((String) value).getBytes());
			} else if (value instanceof byte[]) {
				return new ByteArrayContent(null, (byte[]) value);
			} else {
				throw new IllegalStateException("Cannot handle "
						+ value.getClass());
			}

		}

	}

	private static final HttpRequestFactory requestFactory = createHttpTransport()
			.createRequestFactory();

	private final Proto proto;
	private final String hostname;
	private final int port;

	public GoogleHttpClient(String hostname, int port) {
		this(Proto.HTTP, hostname, port);
	}

	private static HttpTransport createHttpTransport() {
		try {
			// ApacheHttpTransport
			return new NetHttpTransport.Builder().doNotValidateCertificate()
					.build();
		} catch (GeneralSecurityException e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public Builder builder(String uri) {
		return new GoogleBuilder(uri);
	}

	// ----------------------------------------------------------------------------------

	private GenericUrl createGUrl(String uri) throws URISyntaxException {
		return new GenericUrl(new URI(this.proto.getScheme(), null,
				this.hostname, this.port, uri, null, null));
	}

	@Override
	public void close() {
		// TODO do we have to release resources!?
	}

}
