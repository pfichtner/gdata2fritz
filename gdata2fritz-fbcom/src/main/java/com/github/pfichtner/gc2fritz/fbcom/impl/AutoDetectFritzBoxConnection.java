package com.github.pfichtner.gc2fritz.fbcom.impl;

import lombok.RequiredArgsConstructor;

import com.github.pfichtner.gc2fritz.fbcom.api.FritzBoxConnection;
import com.github.pfichtner.gc2fritz.util.Connection;

/**
 * A {@link FritzBoxConnection} that autodetects the type of fritzbox by first
 * trying to login using {@link FritzOsFritzBoxConnection} and after that using
 * a {@link SessionIdFritzBoxConnection}.
 * 
 * @author Peter Fichtner
 */
@RequiredArgsConstructor
public class AutoDetectFritzBoxConnection extends ForwardingFritzBoxConnection {

	private final Connection connection;

	@Override
	public FritzBoxConnection login(String username, String password)
			throws Exception {
		tryLogin(username, password);
		return getDelegate();
	}

	public boolean tryLogin(String username, String password) throws Exception {
		if (tryLogin(new FritzOsFritzBoxConnection(this.connection), username,
				password)) {
			return true;
		}
		if (tryLogin(new SessionIdFritzBoxConnection(this.connection),
				username, password)) {
			return true;
		}
		throw new IllegalStateException("Could not login");
	}

	private boolean tryLogin(FritzBoxConnection connection, String username,
			String password) throws Exception {
		if (!connection.tryLogin(username, password)) {
			return false;
		}
		setDelegate(connection);
		return true;
	}

	@Override
	public void close() {
		FritzBoxConnection delegate = getDelegate();
		if (delegate != null) {
			delegate.close();
		}
	}

}
