package com.github.pfichtner.gc2fritz.fbcom.api;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Map;

public interface FritzBoxConnection extends Closeable {

	/**
	 * Performs a login, throws a RuntimException on login failures.
	 * 
	 * @param username
	 *            the username for the login
	 * @param password
	 *            the password for the login
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws Exception
	 */
	FritzBoxConnection login(String username, String password) throws Exception;

	/**
	 * Tries to perform a login, will not throw an exception but return
	 * <code>false</code> on failure.
	 * 
	 * @param username
	 *            the username for the login
	 * @param password
	 *            the password for the login
	 * 
	 * @return <code>true</code> on login success, <code>false</code> otherwise
	 * @throws Exception
	 */
	boolean tryLogin(String username, String password) throws Exception;

	void uploadPhoneBook(InputStream phonebookContent, int phoneBookId)
			throws Exception;

	void downloadPhoneBook(OutputStream outputStream, int phoneBookId)
			throws Exception;

	void allTelephonesSilent(boolean onOff) throws Exception;

	Collection<String> listCalls() throws UnknownHostException, IOException,
			URISyntaxException;

	Map<Integer, String> listPhoneBooks() throws Exception;

	void close();

}
