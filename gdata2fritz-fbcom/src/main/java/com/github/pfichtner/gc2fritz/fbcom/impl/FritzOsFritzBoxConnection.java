package com.github.pfichtner.gc2fritz.fbcom.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Collection;

import lombok.extern.slf4j.Slf4j;

import com.github.pfichtner.gc2fritz.util.Connection;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

/**
 * Used by FritzBox OS >= 5.50
 * 
 * @author Peter Fichtner
 */
@Slf4j
public class FritzOsFritzBoxConnection extends AbstractFritzBoxConnection {

	public FritzOsFritzBoxConnection(Connection connection)
			throws UnknownHostException, IOException, URISyntaxException {
		super(connection);
	}
	
	
	@Override
	public boolean tryLogin(String username, String password) throws Exception {
		String httpResponse = getChallenge();
		SessionInfo si = toSessionInfo(httpResponse);

		if (CharMatcher.is('0').matchesAllOf(si.getSid())) {
			String challenge = si.getChallenge();
			log.debug("Got challenge {}", challenge);
			String response = getResponse(challenge, password);
			log.debug("Calculated response {}", response);
			httpResponse = getHttpClient().builder("/login_sid.lua")
					.add("username", username).add("response", response)
					.doPost();
		}
		setSid(toSessionInfo(httpResponse).getSid());
		return isSidOk();
	}

	public Collection<String> listCalls() throws UnknownHostException,
			IOException, URISyntaxException {
		String getResponse = getHttpClient()
				.builder("/fon_num/foncalls_list.lua").add("sid", getSid())
				.add("csv", "1").doGet();
		// TODO filter empty lines
		return Lists.newArrayList(Splitter.on(CharMatcher.anyOf("\r\n")).split(
				getResponse));
	}

}
