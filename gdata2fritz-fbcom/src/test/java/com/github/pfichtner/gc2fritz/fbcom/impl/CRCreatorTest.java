package com.github.pfichtner.gc2fritz.fbcom.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class CRCreatorTest {

	@Test
	public void testChallenge() {
		// Example from PDF "Session-IDs im FRITZ!Box Webinterface - AVM"
		assertThat(new CRCreator("äbc").challenge("1234567z"),
				equalTo("1234567z-9e224a41eeefa284df7bb0f26c2913e2"));
	}

}
